#include "AppDelegate.h"
#include "GeneratedPluginRegistrant.h"
#import "GoogleMaps/GoogleMaps.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application
    didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
      // Added in API key for Google Maps plugin
      [GMSServices provideAPIKey:@"AIzaSyD1hFWAmNfxNQTA1JDn8Rutu8WsRdDFp4A"];
      [GeneratedPluginRegistrant registerWithRegistry:self];
  // Override point for customization after application launch.
      return [super application:application didFinishLaunchingWithOptions:launchOptions];
    }

@end
