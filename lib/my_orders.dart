import 'package:crunch_flutter/repositories/order_repository.dart';
import 'package:crunch_flutter/repositories/user_repository.dart';
import 'package:crunch_flutter/shared/user_details.dart';
import 'package:crunch_flutter/tracking.dart';
import 'package:flutter/material.dart';

import 'models/order.dart';

class MyOrders extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new _MyOrdersState();
}

class _MyOrdersState extends State<MyOrders>
    with AutomaticKeepAliveClientMixin {
  UserRepository _userRepository = new UserRepository();
  OrderRepository _orderRepository = new OrderRepository();
  UserDetails userDetails = UserDetails();

  int nextPage = 0;
  List<GestureDetector> cards = new List<GestureDetector>();

  //This function returns the given order's items in a column (for the modal that pops up when the order is clicked)
  Widget getOrderItems(Order order) {
    print(order.toJson());
    List<Text> items = new List<Text>();
    order.orderItems.forEach((orderItem) => items.add(Text(orderItem.itemname +
        (orderItem.size != null ? " - " + orderItem.size : ""))));
    return Column(
      children: items,
    );
  }

  void showDeletionDialog(int orderId) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Confirm Deletion"),
            content: Text(
                "Deleting this order will refund you the appropriate amount"),
            actions: [
              FlatButton(
                child: Text("Close"),
                onPressed: () {
                  Navigator.pop(context, false);
                },
              ),
              FlatButton(
                color: Colors.red,
                child: Text("Delete"),
                onPressed: () {
                  //Pop twice to dismiss dialog and reset My Orders page
                  _orderRepository.deleteOrder(orderId);
                  Navigator.pop(context, false);
                  Navigator.pop(context, false);
                  setState(() {});
                },
              )
            ],
          );
        });
  }

  GestureDetector orderCard(Order order) {
    return GestureDetector(
      onTap: () {
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return SimpleDialog(
                title: Text('Order #${order.orderid}'),
                children: <Widget>[
                  SimpleDialogOption(
                      child: new Column(children: <Widget>[
                    Text("Items:\n"),
                    getOrderItems(order),
                    Divider(color: Colors.black),
                    Text(
                        "Additional Requests: \n\n${order.additionalrequests}"),
                    Divider(color: Colors.black),
                    Text(
                        "Description of user: \n\n${order.customerdescription}"),
                    Divider(color: Colors.black),
                    Text(
                        "Cost of order: \$${(order.ordertotal - order.ordersurcharge).toStringAsFixed(2)}"),
                    if (order.orderstatus == 1)
                      FlatButton(
                        child: Text("Cancel Order"),
                        color: Colors.red,
                        onPressed: () {
                          showDeletionDialog(order.orderid);
                        },
                      ),
                    if (order.orderstatus == 2)
                      FlatButton(
                        child: Text("Tracking Page"),
                        color: Colors.green,
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => TrackingPage(order)));
                        },
                      )
                  ])),
                ],
              );
            });
      },
      child: Card(
        elevation: 8.0,
        margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
        child: Stack(
          children: <Widget>[
            Container(
              height: 100,
              decoration: BoxDecoration(
                color: Colors.transparent,
                image: DecorationImage(
                  alignment: Alignment.centerRight,
                  fit: BoxFit.fitHeight,
                  image: AssetImage(
                    "assets/images/" + order.eatery.eateryName + "_logo.jpg",
                  ),
                ),
              ),
            ),
            Container(
              height: 100,
              decoration: BoxDecoration(
                  color: Colors.white,
                  gradient: LinearGradient(
                      begin: FractionalOffset.centerRight,
                      end: FractionalOffset.centerLeft,
                      colors: [
                        Colors.grey[300].withOpacity(0.5),
                        order.orderstatus == 2
                            ? Colors.orange
                            : (order.orderstatus == 3
                                ? Colors.green
                                : Colors.redAccent),
                      ],
                      stops: [
                        0.0,
                        1.0
                      ])),
              child: ListTile(
                  contentPadding:
                      EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                  leading: Container(
                    padding: EdgeInsets.only(right: 12.0),
                    decoration: new BoxDecoration(
                        border: new Border(
                            right: new BorderSide(
                                width: 1.0, color: Colors.white24))),
                    child: order.orderstatus == 2
                        ? Icon(Icons.directions_walk, color: Colors.white)
                        : (order.orderstatus == 3
                            ? Icon(Icons.check_circle, color: Colors.white)
                            : Icon(Icons.hourglass_full, color: Colors.white)),
                  ),
                  title: Text(
                    "${order.eatery.eateryName} - ${order.orderItems.length} items",
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  ),
                  subtitle: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(children: <Widget>[
                        Icon(Icons.sort, color: Colors.black),
                        Text(" Click for more details",
                            style: TextStyle(color: Colors.white))
                      ]),
                      Text(
                          order.orderstatus == 2
                              ? "\nAccepted - Delivery in Progress"
                              : (order.orderstatus == 3
                                  ? "\nDelivered"
                                  : "\nWaiting for acceptance..."),
                          style: TextStyle(color: Colors.black38),
                          textAlign: TextAlign.left)
                    ],
                  ),
                  trailing: Icon(Icons.keyboard_arrow_right,
                      color: Colors.grey, size: 30.0)),
            )
          ],
        ),
      ),
    );
  }

  //This will create a list of cards that contain all orders.
  //TODO: This needs to be paginated in the future for sure, or at least not loaded in its entirety right away
  Widget allOrders(List<Order> orders) {
    orders.forEach((order) => cards.add(orderCard(order)));
    return new Container(
      child: new ListView.builder(
          itemCount: cards.length,
          itemBuilder: (BuildContext context, int index) {
            return cards[index];
          }),
    );
  }

  Widget allUserOrders(List<int> orderStatus) {
    String useremail = userDetails.user.useremail;
    return FutureBuilder(
        future: _userRepository.getOrdersForUser(useremail, 0),
        //set as AsyncSnapshot because then we can access the actual order list with autocomplete if we need to
        builder: (context, AsyncSnapshot<List<Order>> snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.data == null) {
              return Center(
                child: Column(children: <Widget>[
                  Text("No orders to show :( "),
                  FlatButton(
                      color: Colors.green,
                      child: Text("Reload"),
                      onPressed: () => setState(() {}))
                ]),
              );
            }
            snapshot.data
                .sort((a, b) => b.orderstatus.compareTo(a.orderstatus));
            List<Order> ordersByStatus = snapshot.data
                .where((order) => orderStatus.contains(order.orderstatus))
                .toList();
            cards.clear();
            return allOrders(ordersByStatus);
          } else {
            return new Center(
                child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(Colors.orange),
            ));
          }
        });
  }

  Widget build(BuildContext context) {
    //Creates a page with tabs and items separated by category
    return DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            title: Text("My Orders"),
            centerTitle: true,
            bottom: PreferredSize(
                child: TabBar(
                    isScrollable: true,
                    unselectedLabelColor: Colors.blue.withOpacity(0.3),
                    indicatorColor: Colors.black,
                    tabs: <Widget>[
                      Tab(child: Text("Past Orders")),
                      Tab(child: Text("Current Orders"))
                    ]),
                preferredSize: Size.fromHeight(45.0)),
          ),
          body: TabBarView(children: [
            allUserOrders([3]),
            allUserOrders([1, 2])
          ]),
        ));
  }

  @override
  bool get wantKeepAlive => true;
}
