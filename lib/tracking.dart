/**
 * 
 * tracking.dart
 * 
 * Julian Mooken
 * 
 * This page handles all tracking related functionaltiy (showing eatery location, deliverer location, timer, etc.)
 * 
 */

import 'dart:ui';
import 'dart:async';
import 'dart:io';
import 'package:crunch_flutter/models/order.dart';
import 'package:crunch_flutter/repositories/order_repository.dart';
import 'package:crunch_flutter/shared/user_details.dart';
import 'package:crunch_flutter/values/pickup_locations.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter/material.dart';
import 'package:location/location.dart';
import 'shared/permissions_service.dart';
import 'package:geoflutterfire/geoflutterfire.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class TrackingPage extends StatefulWidget {
  final Order order;
  TrackingPage(this.order);
  @override
  State<StatefulWidget> createState() => new _MyTrackingPageState();
}

class _MyTrackingPageState extends State<TrackingPage> {
  MapType _currentMapType = MapType.normal;
  Timer timer;
  Completer<GoogleMapController> _controller = Completer();
  GoogleMapController mapController;

  OrderRepository _orderRepository = new OrderRepository();

  Firestore firestore = Firestore.instance;
  Geoflutterfire geo = Geoflutterfire();

  var location = new Location();
  var userLocation;
  LatLng _currentPosition;
  LatLng _pickupLocation;
  LatLngBounds bound;
  UserDetails userDetails = UserDetails();
  double distanceBetweenInMeters;
  bool modalCheck;
  LocationData currentLocation;
  GeoPoint runnerLocation;
  LatLng eateryLocation;
  MarkerId eateryMarkerId;
  InfoWindow eateryInfoWindow;

  /// Custom icon stuff is commented out for now, unsure as to why it's not working. ///
  final Set<Marker> _markers = Set();

  // Initialize the page with an asynchronous fxn waiting to retrieve user location data.
  @override
  initState() {
    super.initState();
    PermissionsService().requestLocationPermission();
    modalCheck = true;
    userLocation = _getLocation();
    var pickupLocation = widget.order.orderlocation;
    location.onLocationChanged().listen((location) async {
      userLocation = _getLocation();
      distanceBetweenInMeters = await Geolocator().distanceBetween(
          location.latitude,
          location.longitude,
          _pickupLocation.latitude,
          _pickupLocation.longitude);
      if (distanceBetweenInMeters <= 10 && modalCheck == true) {
        modalCheck = false;
        _confirmOrder();
      }
    });
    // Once user location has been acquired, then set the user's initial position on the map.
    // It may not be set straight away, but the async allows for the promise that it will indeed be set in the future.
    userLocation.then((data) async {
      // currentPosition is basically the lat and long of currentLocation in the _getLocation method.
      _currentPosition = LatLng(data.latitude, data.longitude);
    });
    if (widget.order.eatery.eateryName == "Tom Hortons") {
      eateryLocation = LatLng(PickupLocations.eateryLocationsLat["Tom Hortons"],
          PickupLocations.eateryLocationsLong["Tom Hortons"]);
      eateryMarkerId = MarkerId('tom_hortons');
      eateryInfoWindow = InfoWindow(title: 'Tom Hortons', snippet: 'B-Wing');
    }
    if (widget.order.eatery.eateryName == "Ty Express") {
      eateryLocation = LatLng(PickupLocations.eateryLocationsLat["Ty Express"],
          PickupLocations.eateryLocationsLong["Ty Express"]);
      eateryMarkerId = MarkerId('ty_express');
      eateryInfoWindow = InfoWindow(title: 'Ty Express', snippet: 'B-Wing');
    }
    if (widget.order.eatery.eateryName == "Carvey\'s") {
      eateryLocation = LatLng(PickupLocations.eateryLocationsLat["Carvey\'s"],
          PickupLocations.eateryLocationsLong["Carvey\'s"]);
      eateryMarkerId = MarkerId('carveys');
      eateryInfoWindow = InfoWindow(title: 'Carvey\'s', snippet: 'B-Wing');
    }
    if (widget.order.eatery.eateryName == "Third Cup") {
      eateryLocation = LatLng(PickupLocations.eateryLocationsLat["Third Cup"],
          PickupLocations.eateryLocationsLong["Third Cup"]);
      eateryMarkerId = MarkerId('third_cup');
      eateryInfoWindow =
          InfoWindow(title: 'Third Cup', snippet: 'SCAET Building');
    }
    // We check the orderLocation from the order to see where the order should be picked up from, and then set the lat and long to be used for the pin.
    switch (pickupLocation) {
      case "A Wing":
        {
          _pickupLocation = LatLng(
              PickupLocations.sheridanTrafalgarWingsLat.values.toList()[0],
              PickupLocations.sheridanTrafalgarWingsLong.values.toList()[0]);
        }
        break;

      case "B Wing":
        {
          _pickupLocation = LatLng(
              PickupLocations.sheridanTrafalgarWingsLat.values.toList()[1],
              PickupLocations.sheridanTrafalgarWingsLong.values.toList()[1]);
        }
        break;

      case "C Wing":
        {
          _pickupLocation = LatLng(
              PickupLocations.sheridanTrafalgarWingsLat.values.toList()[2],
              PickupLocations.sheridanTrafalgarWingsLong.values.toList()[2]);
        }
        break;

      case "D Wing":
        {
          _pickupLocation = LatLng(
              PickupLocations.sheridanTrafalgarWingsLat.values.toList()[3],
              PickupLocations.sheridanTrafalgarWingsLong.values.toList()[3]);
        }
        break;

      case "E Wing":
        {
          _pickupLocation = LatLng(
              PickupLocations.sheridanTrafalgarWingsLat.values.toList()[4],
              PickupLocations.sheridanTrafalgarWingsLong.values.toList()[4]);
        }
        break;

      case "G Wing":
        {
          _pickupLocation = LatLng(
              PickupLocations.sheridanTrafalgarWingsLat.values.toList()[5],
              PickupLocations.sheridanTrafalgarWingsLong.values.toList()[5]);
        }
        break;

      case "H Wing":
        {
          _pickupLocation = LatLng(
              PickupLocations.sheridanTrafalgarWingsLat.values.toList()[6],
              PickupLocations.sheridanTrafalgarWingsLong.values.toList()[6]);
        }
        break;

      case "J Wing":
        {
          _pickupLocation = LatLng(
              PickupLocations.sheridanTrafalgarWingsLat.values.toList()[7],
              PickupLocations.sheridanTrafalgarWingsLong.values.toList()[7]);
        }
        break;

      case "K Wing":
        {
          _pickupLocation = LatLng(
              PickupLocations.sheridanTrafalgarWingsLat.values.toList()[8],
              PickupLocations.sheridanTrafalgarWingsLong.values.toList()[8]);
        }
        break;
      case "Commons":
        {
          _pickupLocation = LatLng(
              PickupLocations.sheridanTrafalgarWingsLat.values.toList()[9],
              PickupLocations.sheridanTrafalgarWingsLong.values.toList()[9]);
        }
    }
  }

  Future<void> _createMarkerImageFromAsset(String iconPath, MarkerId markerId,
      LatLng position, InfoWindow infoWindowTitle) async {
    ImageConfiguration imgConfig = ImageConfiguration();
    BitmapDescriptor icon =
        await BitmapDescriptor.fromAssetImage(imgConfig, iconPath);
    _markers.add(Marker(
        markerId: markerId,
        position: LatLng(position.latitude, position.longitude),
        icon: icon,
        infoWindow: infoWindowTitle));
  }

  // This fxn will wait until the device has finished retrieving location data.
  Future<LocationData> _getLocation() async {
    try {
      // Will return a Future<LocationData>.
      // The await will cast the Future into the proper variable type when it receives data.
      currentLocation = await location.getLocation();
    } catch (e) {
      print("current location not acquired");
      currentLocation = null;
    }
    return currentLocation;
  }

  Future<void> _realTimeLocation(LatLng currentPosition) async {
    timer = Timer.periodic(Duration(milliseconds: 3000),
        (Timer t) => _updateLocation(userDetails));
  }

  void _onMapTypeButtonPressed() {
    setState(() {
      _currentMapType = _currentMapType == MapType.normal
          ? MapType.satellite
          : MapType.normal;
    });
  }

  void check(CameraUpdate u, GoogleMapController c) async {
    c.animateCamera(u);
    mapController.animateCamera(u);
    LatLngBounds l1 = await c.getVisibleRegion();
    LatLngBounds l2 = await c.getVisibleRegion();
    if (l1.southwest.latitude == -90 || l2.southwest.latitude == -90)
      check(u, c);
  }

  Future<void> _onMapCreated(GoogleMapController controller) async {
    mapController = controller;
    _controller.complete(controller);
    _setBounds();
    _addGeoPoint(userDetails, _currentPosition);
    _realTimeLocation(_currentPosition);
    _createMarkerImageFromAsset(
        "assets/images/pickup_icon.png",
        MarkerId("pickup_location"),
        _pickupLocation,
        InfoWindow(title: "Pickup Location"));
    _createMarkerImageFromAsset("assets/images/eatery_icon.png", eateryMarkerId,
        eateryLocation, eateryInfoWindow);
    _moveToPosition();
    // if (userDetails.user.useremail == widget.order.user.useremail) {
    _addRunnerMarker();
    // }
  }

  Future<void> _setBounds() async {
    var newSW;
    var newNE;
    var updatedPosition = await location.getLocation();
    // sleep(const Duration(milliseconds: 800));
    _currentPosition =
        LatLng(updatedPosition.latitude, updatedPosition.longitude);
    if (_currentPosition.latitude <= _pickupLocation.latitude) {
      if (_currentPosition.longitude <= _pickupLocation.longitude) {
        bound = LatLngBounds(
            southwest: _currentPosition, northeast: _pickupLocation);
      } else {
        newSW = LatLng(_currentPosition.latitude, _pickupLocation.longitude);
        newNE = LatLng(_pickupLocation.latitude, _currentPosition.longitude);
        bound = LatLngBounds(southwest: newSW, northeast: newNE);
      }
    } else {
      if (_pickupLocation.longitude <= _currentPosition.longitude) {
        bound = LatLngBounds(
            southwest: _pickupLocation, northeast: _currentPosition);
      } else {
        newSW = LatLng(_pickupLocation.latitude, _currentPosition.longitude);
        newNE = LatLng(_currentPosition.latitude, _pickupLocation.longitude);
        bound = LatLngBounds(southwest: newSW, northeast: newNE);
      }
    }
  }

  void _moveToPosition() {
    if (bound != null) {
      CameraUpdate u2 = CameraUpdate.newLatLngBounds(bound, 50);
      this.mapController.animateCamera(u2).then((void v) {
        check(u2, this.mapController);
      });
    }
  }

  Future<void> _confirmOrder() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return SimpleDialog(
          title: Text('Confirm Order', textAlign: TextAlign.center),
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: Text(
                  'The other user is near you. Tap the button below to confirm your delivery!',
                  textAlign: TextAlign.center),
            ),
            SimpleDialogOption(
              child: FlatButton(
                child: Text(
                  'OK',
                  style: TextStyle(color: Colors.lightBlue, fontSize: 22),
                ),
                onPressed: () {
                  if (userDetails.user.useremail ==
                      widget.order.user.useremail) {
                    firestore
                        .collection('order_confirmations')
                        .document(widget.order.orderid.toString())
                        .setData({'userCheck': true}, merge: true);
                    Navigator.of(context).pop();
                    _checkConfirmation();
                  } else {
                    firestore
                        .collection('order_confirmations')
                        .document(widget.order.orderid.toString())
                        .setData({'runnerCheck': true}, merge: true);
                    Navigator.of(context).pop();
                    _checkConfirmation();
                  }
                },
              ),
            ),
            SimpleDialogOption(
                child: FlatButton(
              child: Text(
                  userDetails.user.useremail == widget.order.user.useremail
                      ? 'Runner never showed up?'
                      : 'Orderer not present?'),
              onPressed: () {
                if (userDetails.user.useremail == widget.order.user.useremail) {
                  //send orderer help email
                } else {
                  //send runner help email
                }
              },
            ))
          ],
        );
      },
    );
  }

  _checkConfirmation() {
    showDialog<void>(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
              title: Text('Order Complete'),
              content: new StreamBuilder(
                  stream: Firestore.instance
                      .collection('order_confirmations')
                      .document(widget.order.orderid.toString())
                      .snapshots(),
                  builder: (context, snapshot) {
                    if (!snapshot.hasData ||
                        (snapshot.data['userCheck'] != true ||
                            snapshot.data['runnerCheck'] != true)) {
                      return Column(
                        children: <Widget>[
                          new Text(
                              "Waiting for other party to confirm delivery"),
                          new Center(
                              child: CircularProgressIndicator(
                            valueColor:
                                AlwaysStoppedAnimation<Color>(Colors.orange),
                          ))
                        ],
                      );
                    }
                    _orderRepository.deliverOrder(widget.order.orderid);
                    return Column(
                      children: <Widget>[
                        new Text("Ordered successfully delivered!"),
                        new Icon(
                          Icons.check_circle,
                          color: Colors.green,
                          size: 30.0,
                        ),
                      ],
                    );
                  }),
              actions: <Widget>[
                RaisedButton(
                  color: Colors.orange,
                  textColor: Colors.white,
                  child: Text('Home Page'),
                  onPressed: () {
                    Navigator.pushNamedAndRemoveUntil(
                        context, "/home", (_) => false);
                  },
                ),
              ]);
        });
  }

  // Set GeoLocation Data
  Future<void> _addGeoPoint(
      UserDetails userDetails, LatLng currentPosition) async {
    var pos = currentPosition;
    GeoFirePoint point =
        geo.point(latitude: pos.latitude, longitude: pos.longitude);
    var result;
    if (widget.order.user.useremail != userDetails.user.useremail) {
      result = firestore
          .collection('locations')
          .document(widget.order.orderid.toString())
          .setData({'position': point.data, 'name': 'runner location'});
    }
    return result;
  }

  Future<void> _addRunnerMarker() async {
    geo
        .collection(collectionRef: firestore.collection('locations'))
        .data(widget.order.orderid.toString())
        .forEach((List<DocumentSnapshot> documentList) {
      documentList.forEach((DocumentSnapshot document) {
        runnerLocation = document.data['position']['geopoint'];

        if (this.mounted) {
          setState(() {
            _createMarkerImageFromAsset(
                "assets/images/runner_icon.png",
                MarkerId("runner_location"),
                LatLng(runnerLocation.latitude, runnerLocation.longitude),
                InfoWindow(title: 'Runner Location'));
          });
        }
      });
    });
  }

  Future<void> _updateLocation(UserDetails userDetails) async {
    var result;
    if (widget.order.orderstatus != 3) {
      var pos = await location.getLocation();
      GeoFirePoint point =
          geo.point(latitude: pos.latitude, longitude: pos.longitude);
      if (widget.order.user.useremail != userDetails.user.useremail) {
        result = firestore
            .collection('locations')
            .document(widget.order.orderid.toString())
            .updateData({'position': point.data, 'name': 'runner location'});
      }
    } else {
      timer.cancel();
    }
    return result;
  }

  Future<void> executeThisAfterBuild() async {
    _setBounds();
    WidgetsBinding.instance.addPostFrameCallback((_) => _moveToPosition());
  }

// Creates a SingleChildScrollView with the RegisterForm inside of it.
  @override
  Widget build(BuildContext context) {
    // sleep(const Duration(milliseconds: 1000));
    executeThisAfterBuild();
    return WillPopScope(
      child: new Scaffold(
        appBar: new AppBar(
          leading: new IconButton(
              icon: new Icon(Icons.arrow_back),
              onPressed: () async {
                Navigator.pop(context, false);
                Navigator.pop(context, false);
              }),
          title: new Text("Track Your Order"),
        ),
        body: Stack(
          children: <Widget>[
            // We are not showing the map until userLocation has data.
            FutureBuilder<LocationData>(
                future: userLocation,
                // Same fxnality as the Build fxn that this is a part of.
                builder:
                    // snapshot is the current state of userLocation's data.
                    // It may or may not be null.
                    (BuildContext context,
                        AsyncSnapshot<LocationData> snapshot) {
                  // If the snapshot has the userLocation data, we can go ahead and build the map.
                  // Otherwise, display a loading symbol.
                  if (snapshot.hasData || userLocation != null) {
                    return Stack(children: [
                      GoogleMap(
                        mapType: _currentMapType,
                        scrollGesturesEnabled: true,
                        rotateGesturesEnabled: true,
                        tiltGesturesEnabled: true,
                        myLocationEnabled: true,
                        myLocationButtonEnabled: true,
                        compassEnabled: true,
                        zoomGesturesEnabled: true,
                        initialCameraPosition: CameraPosition(
                          target: _currentPosition ??
                              new LatLng(43.4695468, -79.7006452),
                          zoom: 11.0,
                        ),
                        onMapCreated: _onMapCreated,
                        markers: _markers,
                      ),
                    ]);
                  } else {
                    return new Center(
                        child: CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation<Color>(Colors.orange),
                    ));
                  }
                }),
            Padding(
              /// In case we remove default recentre button
              // padding: const EdgeInsets.all(16.0),
              padding: const EdgeInsets.fromLTRB(16.0, 64.0, 8.0, 16.0),
              child: Align(
                alignment: Alignment.topRight,
                child: FloatingActionButton(
                  onPressed: () {
                    _onMapTypeButtonPressed();
                  },
                  materialTapTargetSize: MaterialTapTargetSize.padded,
                  backgroundColor: Colors.orange,
                  child: const Icon(Icons.map, size: 36.0),
                ),
              ),
            ),
          ],
        ),
        floatingActionButton: FloatingActionButton.extended(
          onPressed: _moveToPosition,
          label: Text('Recentre'),
          icon: Icon(Icons.center_focus_strong),
        ),
      ),
      onWillPop: () {
        return new Future(() => false);
      },
    );
  }
}
