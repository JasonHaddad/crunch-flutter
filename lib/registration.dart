/**
 * 
 * order_list.dart
 * 
 * Julian Mooken
 * 
 * The page allows new users to register an account for the application. Upon completion, they are sent back to the login page
 * 
 */

import 'dart:ui';
import 'package:crunch_flutter/login.dart';
import 'package:crunch_flutter/shared/general_app_bar.dart';
import 'package:crunch_flutter/shared/register_success_dialog.dart';
import 'package:flutter/material.dart';
import 'package:crunch_flutter/models/user.dart';
import 'package:dbcrypt/dbcrypt.dart';
import 'shared/validate_extension.dart';
import 'package:crunch_flutter/repositories/user_repository.dart';

class RegistrationPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new _MyRegistrationPageState();
}

class _MyRegistrationPageState extends State<RegistrationPage> {
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

// Creates a SingleChildScrollView with the RegisterForm inside of it.
  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;

    return new Scaffold(
      appBar: GeneralAppBar("Registration").setAppBar(),
      body: const SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 8.0),
          child: RegisterForm(),
        ),
      ),
    );
  }
}

//This class will simply store our registration info to be passed to the 'register' function eventually
class RegistrationData {
  String firstName = '';
  String lastName = '';
  String phoneNumber = '';
  String email = '';
  String password = '';
  String salt = '';
}

//Three validation paramters needed to pass registration
String validateFirstName(String value) {
  try {
    ValidateExtension.isLetters(value);
  } catch (e) {
    return 'Invalid First Name. Please use only letters.';
  }
  return null;
}

String validateLastName(String value) {
  try {
    ValidateExtension.isLetters(value);
  } catch (e) {
    return 'Invalid Last Name. Please use only letters.';
  }
  return null;
}

String validatePhoneNumber(String value) {
  return null;
}

class RegisterForm extends StatefulWidget {
  const RegisterForm({Key key}) : super(key: key);

  @override
  _RegisterFormState createState() => _RegisterFormState();
}

class _RegisterFormState extends State<RegisterForm> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  RegistrationData data = new RegistrationData();
  bool _agreedToTOS = false;

  //Function to check that data is validated, then makes a new user object (hashing password with salt)
  //and sending to DB
  void _submit() async {
    int statusCode;
    UserRepository _userRepository = new UserRepository();
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      DBCrypt dBCrypt = DBCrypt();
      String salt = dBCrypt.gensaltWithRounds(12);
      var user = new User(
          userfirstname: data.firstName,
          userlastname: data.lastName,
          useremail: data.email,
          userphonenumber: data.phoneNumber,
          userpassword: dBCrypt.hashpw(data.password, salt),
          usersalt: salt,
          attemptcount: 0);
      statusCode = await _userRepository.register(user);
      if (statusCode == 200) {
        await showDialog(
            context: context,
            builder: (_) => RegisterSuccessDialog(
                  image: Image.network(
                    "https://raw.githubusercontent.com/Shashank02051997/FancyGifDialog-Android/master/GIF's/gif16.gif",
                    fit: BoxFit.cover,
                  ),
                  title: Text(
                    'Success',
                    textAlign: TextAlign.center,
                    style:
                        TextStyle(fontSize: 22.0, fontWeight: FontWeight.w600),
                  ),
                  description: Text(
                    'You are now successfully registered! Go ahead and login now.',
                    textAlign: TextAlign.center,
                  ),
                )).then((val) {
          Navigator.pushNamedAndRemoveUntil(context, "/login", (_) => false);
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    return Form(
        key: _formKey,
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Container(
                  padding: new EdgeInsets.all(10.0),
                  child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      // First Name
                      new TextFormField(
                          autofocus: false,
                          keyboardType: TextInputType.text,
                          decoration: new InputDecoration(
                              labelText: 'First Name',
                              contentPadding:
                                  EdgeInsets.fromLTRB(0, 15.0, 0, 15.0),
                              enabledBorder: UnderlineInputBorder(
                                  borderSide:
                                      new BorderSide(color: Colors.grey))),
                          validator: validateFirstName,
                          onSaved: (String value) {
                            data.firstName = value;
                          }),
                      SizedBox(height: 25.0),
                      // Last Name
                      new TextFormField(
                          autofocus: false,
                          keyboardType: TextInputType.text,
                          decoration: new InputDecoration(
                              labelText: 'Last Name',
                              contentPadding:
                                  EdgeInsets.fromLTRB(0, 15.0, 0, 15.0),
                              enabledBorder: UnderlineInputBorder(
                                  borderSide:
                                      new BorderSide(color: Colors.grey))),
                          validator: validateLastName,
                          onSaved: (String value) {
                            data.lastName = value;
                          }),
                      SizedBox(height: 25.0),
                      // Phone Number
                      new TextFormField(
                          autofocus: false,
                          keyboardType: TextInputType
                              .number, // Use number input for ease of use
                          decoration: new InputDecoration(
                              labelText: 'Phone Number',
                              contentPadding:
                                  EdgeInsets.fromLTRB(0, 15.0, 0, 15.0),
                              enabledBorder: UnderlineInputBorder(
                                  borderSide:
                                      new BorderSide(color: Colors.grey))),
                          validator: validatePhoneNumber,
                          onSaved: (String value) {
                            data.phoneNumber = value;
                          }),
                      SizedBox(height: 25.0),
                      // Email
                      new TextFormField(
                          autofocus: false,
                          keyboardType: TextInputType
                              .emailAddress, // Use email input type for emails.
                          decoration: new InputDecoration(
                              labelText: 'E-mail Address',
                              contentPadding:
                                  EdgeInsets.fromLTRB(0, 15.0, 0, 15.0),
                              enabledBorder: UnderlineInputBorder(
                                  borderSide:
                                      new BorderSide(color: Colors.grey))),
                          validator: validateEmail,
                          onSaved: (String value) {
                            data.email = value;
                          }),
                      SizedBox(height: 25.0),
                      // Password 1
                      new TextFormField(
                          obscureText: true,
                          autofocus: false,
                          keyboardType: TextInputType
                              .emailAddress, // Use email input b/c password can have special characters
                          decoration: new InputDecoration(
                              labelText: 'Password',
                              contentPadding:
                                  EdgeInsets.fromLTRB(0, 15.0, 0, 15.0),
                              enabledBorder: UnderlineInputBorder(
                                  borderSide:
                                      new BorderSide(color: Colors.grey))),
                          validator: validatePassword,
                          onSaved: (String value) {
                            data.password = value;
                          }),
                      SizedBox(height: 25.0),
                      // Password 2
                      new TextFormField(
                          obscureText: true,
                          autofocus: false,
                          keyboardType: TextInputType
                              .emailAddress, // Use email input b/c password can have special characters
                          decoration: new InputDecoration(
                              labelText: 'Confirm Password',
                              contentPadding:
                                  EdgeInsets.fromLTRB(0, 15.0, 0, 15.0),
                              enabledBorder: UnderlineInputBorder(
                                  borderSide:
                                      new BorderSide(color: Colors.grey))),
                          validator: validatePassword,
                          onSaved: (String value) {
                            data.password = value;
                          }),
                      SizedBox(height: 25.0),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 16.0),
                        child: Row(
                          children: <Widget>[
                            Checkbox(
                              value: _agreedToTOS,
                              onChanged: _setAgreedToTOS,
                            ),
                            Expanded(
                              child: GestureDetector(
                                  onTap: () => _setAgreedToTOS(!_agreedToTOS),
                                  child: Container(
                                    width: screenSize.width,
                                    child: const Text(
                                      'I agree to the Terms of Services and Privacy Policy',
                                    ),
                                  )),
                            ),
                          ],
                        ),
                      ),
                      Center(
                        child: OutlineButton(
                          highlightedBorderColor: Colors.black,
                          onPressed: () => _submittable() ? _submit() : null,
                          child: const Text('Register'),
                        ),
                      ),
                    ],
                  )),
            ]));
  }

  //Whether or not they accept terms of service
  bool _submittable() {
    return _agreedToTOS;
  }

  void _setAgreedToTOS(bool newValue) {
    setState(() {
      _agreedToTOS = newValue;
    });
  }
}
