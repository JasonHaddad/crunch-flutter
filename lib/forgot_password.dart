/**
 * 
 * forgot_password.dart.dart
 * 
 * Jason Haddad
 * 
 * The page simply allows the user to enter their email and be sent a password reset link
 * 
 */

import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:validate/validate.dart';

class ForgotPassword extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new _ForgotPasswordState();
}

String validateEmail(String value) {
  // If empty value, the isEmail function throws a error.
  // So I changed this function with try and catch.
  try {
    Validate.isEmail(value);
  } catch (e) {
    return 'Invalid e-mail address.';
  }
  return null;
}

class _ForgotPasswordState extends State<ForgotPassword> {
  void submit() async {
    // First validate form.
    if (this._formKey.currentState.validate()) {
      //TODO: SEND EMAIL HERE

      await showDialog(
          context: context,
          builder: (BuildContext context) {
            return SimpleDialog(
              children: <Widget>[
                Icon(Icons.check_circle, color: Colors.orange),
                SimpleDialogOption(
                  child: new Text(
                      "An email has been sent to the address we have on file. Please read it for further instructions.",
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 18.0)),
                ),
              ],
            );
          }).then((val) {
        Navigator.pushNamedAndRemoveUntil(context, "/login", (_) => false);
      });
    }
  }

  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;

    return new Scaffold(
        body: new Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/images/Artboard 1.png"),
                    fit: BoxFit.cover)),
            child: new Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Hero(
                    tag: 'hero',
                    child: CircleAvatar(
                      backgroundColor: Colors.transparent,
                      radius: 48.0,
                      child: Image.asset('assets/images/logo.png'),
                    ),
                  ),
                  new Container(
                      padding: new EdgeInsets.all(32.0),
                      child: Card(
                        elevation: 10.0,
                        child: Padding(
                          padding: const EdgeInsets.all(15.0),
                          child: new Form(
                            key: this._formKey,
                            child: new Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                new Text(
                                  "Hey, it happens! Just enter your email and we'll send you a password reset link",
                                  textAlign: TextAlign.center,
                                ),
                                new SizedBox(height: 15.0),
                                new Divider(
                                  color: Colors.grey,
                                ),
                                new TextFormField(
                                    autofocus: false,
                                    keyboardType: TextInputType
                                        .emailAddress, // Use email input type for emails.
                                    decoration: new InputDecoration(
                                        labelText: 'E-mail Address',
                                        contentPadding: EdgeInsets.fromLTRB(
                                            0, 15.0, 0, 15.0),
                                        enabledBorder: UnderlineInputBorder(
                                            borderSide: new BorderSide(
                                                color: Colors.grey))),
                                    validator: validateEmail,
                                    onSaved: (String value) {}),
                                new Container(
                                  width: screenSize.width,
                                  child: new RaisedButton(
                                      child: new Text('Send',
                                          style: new TextStyle(
                                              color: Colors.white)),
                                      onPressed: this.submit,
                                      color: Colors.orange,
                                      shape: new RoundedRectangleBorder(
                                          borderRadius:
                                              new BorderRadius.circular(30.0))),
                                  margin: new EdgeInsets.only(top: 20.0),
                                ),
                              ],
                            ),
                          ),
                        ),
                      )),
                ])));
  }
}
