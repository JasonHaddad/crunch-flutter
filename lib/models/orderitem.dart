class OrderItem {
  int id;
  int itemid;
  double itemprice;
  String milkcream;
  String size;
  String itemname;
  String additionalrequests;

  OrderItem({this.id, this.itemid, this.itemprice, this.milkcream, this.size, this.itemname, this.additionalrequests});

  OrderItem.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    itemid = json['itemid'];
    itemprice = json['itemprice'];
    milkcream = json['milkcream'];
    size = json['size'];
    itemname = json['itemname'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['itemid'] = this.itemid;
    data['itemprice'] = this.itemprice;
    data['milkcream'] = this.milkcream;
    data['size'] = this.size;
    return data;
  }
}