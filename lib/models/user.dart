import 'order.dart';

class User {
  String userfirstname;
  String userlastname;
  String useremail;
  String userphonenumber;
  String userpassword;
  String usersalt;
  String userlocation;
  int attemptcount;
  List<Order> orders;
  List<Order> runnerOrder;

  //PURELY to store FCM token, no other purpose beyond this, not stored in DB, for UserDetails singleton
  String userToken;

  User({
      this.userfirstname,
      this.userlastname,
      this.useremail,
      this.userphonenumber,
      this.userpassword,
      this.usersalt,
      this.userlocation,
      this.orders,
      this.runnerOrder,
      this.attemptcount
      });

  User.fromJson(Map<String, dynamic> json) {
    userfirstname = json['userfirstname'];
    userlastname = json['userlastname'];
    useremail = json['useremail'];
    userphonenumber = json['userphonenumber'];
    userpassword = json['userpassword'];
    usersalt = json['usersalt'];
    attemptcount = json['attemptcount'];
    userlocation = json['userlocation'];
    if (json['orders'] != null) {
      orders = new List<Order>();
      json['orders'].forEach((v) {
        orders.add(new Order.fromJson(v));
      });
    }
    if (json['runnerOrder'] != null) {
      orders = new List<Order>();
      json['runnerOrder'].forEach((v) {
        orders.add(new Order.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['userfirstname'] = this.userfirstname;
    data['userlastname'] = this.userlastname;
    data['useremail'] = this.useremail;
    data['userphonenumber'] = this.userphonenumber;
    data['userpassword'] = this.userpassword;
    data['usersalt'] = this.usersalt;
    data['userlocation'] = this.userlocation;
    data['attemptcount'] = this.attemptcount;
    if (this.orders != null) {
      data['orders'] = this.orders.map((v) => v.toJson()).toList();
    }
    if (this.runnerOrder != null) {
      data['runnerOrder'] = this.orders.map((v) => v.toJson()).toList();
    }
    return data;
  }
}