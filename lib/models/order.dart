import 'package:crunch_flutter/models/eatery.dart';
import 'package:crunch_flutter/models/user.dart';

import 'orderitem.dart';

class Order {
  int orderid;
  String ordertimestamp;
  double ordertotal;
  double ordersurcharge;
  List<OrderItem> orderItems;
  Eatery eatery;
  int orderstatus;
  String additionalrequests;
  String orderlocation;
  String usertoken;
  String customerdescription;
  User user;
  User runner;

  Order(
      {this.orderid,
      this.ordertimestamp,
      this.ordertotal,
      this.ordersurcharge,
      this.orderItems,
      this.orderstatus,
      this.additionalrequests,
      this.orderlocation,
      this.eatery,
      this.usertoken,
      this.customerdescription
      });

  Order.fromJson(Map<String, dynamic> json) {
    orderid = json['orderid'];
    ordertimestamp = json['ordertimestamp'];
    ordertotal = json['ordertotal'];
    ordersurcharge = json['ordersurcharge'];
    orderstatus = json['orderstatus'];
    additionalrequests = json['additionalrequests'];
    orderlocation = json['orderlocation'];
    usertoken = json['usertoken'];
    eatery = Eatery.fromJson(json['eatery']);
    user = User.fromJson(json['user']);
    customerdescription = json['customerdescription'];
    //runner = User.fromJson(json['runner']);
    if (json['orderItems'] != null) {
      orderItems = new List<OrderItem>();
      json['orderItems'].forEach((v) {
        orderItems.add(new OrderItem.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['orderid'] = this.orderid;
    data['ordertimestamp'] = this.ordertimestamp;
    data['ordertotal'] = this.ordertotal;
    data['ordersurcharge'] = this.ordersurcharge;
    data['orderstatus'] = this.orderstatus;
    data['additionalrequests'] = this.additionalrequests;
    data['orderlocation'] = this.orderlocation;
    data['eatery'] = this.eatery;
    data['usertoken'] = this.usertoken;
    data['customerdescription'] = this.customerdescription;
    if (this.orderItems != null) {
      data['orderItems'] = this.orderItems.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
