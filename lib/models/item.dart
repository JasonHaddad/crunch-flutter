import 'package:crunch_flutter/models/orderitem.dart';

class Item {
  int categoryid;
  int itemid;
  String itemname;
  List<OrderItem> itemModifications;
  String imageLink;

  Item({this.categoryid, this.itemid, this.itemname});

  Item.fromJson(Map<String, dynamic> json) {
    categoryid = json['categoryid'];
    itemid = json['itemid'];
    itemname = json['itemname'];
    imageLink = json['imageLink'];
    if (json['itemModifications'] != null) {
      itemModifications = new List<OrderItem>();
      json['itemModifications'].forEach((v) {
        itemModifications.add(new OrderItem.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['categoryid'] = this.categoryid;
    data['itemid'] = this.itemid;
    data['itemname'] = this.itemname;
    data['imageLink'] = this.imageLink;
    if (this.itemModifications != null) {
      data['itemModifications'] = this.itemModifications.map((v) => v.toJson()).toList();
    }
    return data;
  }
}