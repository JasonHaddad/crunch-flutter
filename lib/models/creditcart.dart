class CreditCard{

  CreditCard(this.cardnumber, this.cardexpiry, this.cardHolderName, this.cardccv, this.isCvvFocused);

  int cardnumber;
  String cardexpiry;
  String cardHolderName;
  int cardccv;
  bool isCvvFocused = false;

  CreditCard.fromJson(Map<String, dynamic> json) {
    cardnumber = json['cardnumber'];
    cardexpiry = json['cardexpiry'];
    cardccv = json['cardccv'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['cardnumber'] = this.cardnumber;
    data['cardexpiry'] = this.cardexpiry;
    data['cardccv'] = this.cardccv;
    return data;
  }
}
