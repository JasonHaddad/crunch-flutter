class Eatery {
  String eateryName;
  int eateryid;
  String location;

  Eatery({this.eateryName, this.eateryid, this.location});

  Eatery.fromJson(Map<String, dynamic> json) {
    eateryName = json['eateryName'];
    eateryid = json['eateryid'];
    location = json['location'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['eateryName'] = this.eateryName;
    data['eateryid'] = this.eateryid;
    data['location'] = this.location;
    return data;
  }
}