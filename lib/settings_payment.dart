import 'package:crunch_flutter/models/creditcart.dart';
import 'package:crunch_flutter/repositories/creditcard_repository.dart';
import 'package:crunch_flutter/shared/credit_card_form.dart';
/**
 * 
 * settings_payment.dart
 * 
 * Julian Mooken
 * 
 * The page allows users to add a credit card and see the credit cards they have added
 * 
 */

import 'package:crunch_flutter/shared/general_app_bar.dart';
import 'package:flutter/material.dart';

class SettingsPayment extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _SettingsPaymentState();
  }
}

class _SettingsPaymentState extends State<SettingsPayment> {
  final CreditCardRepository _creditCardRepository = new CreditCardRepository();

  ListView cardList(List<CreditCard> cards) {
    return new ListView.builder(
        itemCount: cards.length,
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemBuilder: (BuildContext context, int index) {
          return ListTile(
            title: Text(cards[index].cardnumber.toString()),
            onTap: () {},
            trailing: Text(cards[index].cardexpiry),
          );
        });
  }

  int cardNumber;
  String expiryDate = '';
  String cardHolderName = '';
  int cvvCode;
  bool isCvvFocused = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              FutureBuilder(
                  future: _creditCardRepository.getCards(),

                  //set as AsyncSnapshot because then we can access the actual order list with autocomplete if we need to
                  builder: (context, AsyncSnapshot<List<CreditCard>> snapshot) {
                    if (snapshot.connectionState == ConnectionState.done) {
                      if (snapshot?.data?.length == 0) {
                        return new Center(
                            child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                              "Doesn't look like you've added any cards. Add one below!"),
                        ));
                      }
                      return Column(children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            "Your cards",
                            textAlign: TextAlign.center,
                            style: TextStyle(fontSize: 25),
                          ),
                        ),
                        cardList(snapshot.data)
                      ]);
                    }
                    return new Center(
                        child: CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation<Color>(Colors.orange),
                    ));
                  }),
              Divider(
                color: Colors.black,
                thickness: 1,
              ),
              Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    "Add a card",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 25),
                  )),
              CreditCardForm(
                themeColor: Colors.red,
                resetState: resetState,
              )
            ],
          ),
        ),
        appBar: GeneralAppBar("Payment").setAppBar());
  }

  resetState() {
    setState(() {});
  }
}
