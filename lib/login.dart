/**
 * 
 * login.dart
 * 
 * Jason Haddad
 * 
 * This page is the initial screen for the application (if they have not yet logged in or have just logged out). From here they can
 * register an account, go through the 'Forgot my Password' route, or log into their account.
 * 
 */

import 'dart:ui';
import 'dart:convert';
import 'package:crunch_flutter/models/user.dart';
import 'package:crunch_flutter/shared/user_details.dart';

import 'repositories/user_repository.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:validate/validate.dart';

class MyLoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new _MyLoginPageState();
}

String validateEmail(String value) {
  // If empty value, the isEmail function throws a error.
  // So I changed this function with try and catch.
  try {
    Validate.isEmail(value);
  } catch (e) {
    return 'Invalid e-mail address.';
  }
  return null;
}

String validatePassword(String value) {
  if (value.length < 8) {
    return 'Password is too short.';
  }
  return null;
}

class _MyLoginPageState extends State<MyLoginPage> {
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

  //Used to handle login functionality
  UserRepository _userRepository = new UserRepository();

  //Singleton of our user details
  UserDetails userDetails = UserDetails();

  //Results from form submission
  String email;
  String password;

  void submit() {
    // First, validate form...
    if (this._formKey.currentState.validate()) {
      _formKey.currentState.save(); // Save our form now.

      /**
       * It's dirty, but we try and login (essentially find a user by email AND hashed password).
       * We check if the user has a password (we can check anything, just to see if we have data).
       * If it doesn't, it means wrong credentials, otherwise save user info and login
       * **/
      var user = _userRepository.login(email, password);
      user.then((data) async {
        if (data.userpassword != null) {
          //Save our user in shared_preferences
          SharedPreferences prefs = await SharedPreferences.getInstance();
          prefs.setString('user', json.encode(data));

          //Set up our global user singleton (cleaner than constantly calling SharedPrefs even though they are both singletons)
          UserDetails userDetails = UserDetails();
          userDetails.user = User.fromJson(json.decode(prefs.getString("user")));
          Navigator.pushReplacementNamed(context, '/home');
        } else {
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return SimpleDialog(
                  children: <Widget>[
                    SimpleDialogOption(
                      child: new Text("Incorrect credentials!"),
                    ),
                  ],
                );
              });
        }
      });
    }
  }

  // Navigates us to the Registration Page
  void _navigateToRegistration(BuildContext context) {
    Navigator.pushNamed(context, '/register');
  }

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;

    return new Scaffold(
        body: new Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/images/Artboard 1.png"),
                    fit: BoxFit.cover)),
            child: new Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Hero(
                    tag: 'hero',
                    child: CircleAvatar(
                      backgroundColor: Colors.transparent,
                      radius: 48.0,
                      child: Image.asset('assets/images/logo.png'),
                    ),
                  ),
                  new Container(
                      padding: new EdgeInsets.all(32.0),
                      child: Card(
                        elevation: 10.0,
                        child: Padding(
                          padding: const EdgeInsets.all(15.0),
                          child: new Form(
                            key: this._formKey,
                            child: new Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                new TextFormField(
                                    autofocus: false,
                                    keyboardType: TextInputType
                                        .emailAddress, // Use email input type for emails.
                                    decoration: new InputDecoration(
                                        labelText: 'E-mail Address',
                                        contentPadding: EdgeInsets.fromLTRB(
                                            0, 15.0, 0, 15.0),
                                        enabledBorder: UnderlineInputBorder(
                                            borderSide: new BorderSide(
                                                color: Colors.grey))),
                                    validator: validateEmail,
                                    onSaved: (String value) {
                                      this.email = value;
                                    }),
                                SizedBox(height: 25.0),
                                new TextFormField(
                                    obscureText: true,
                                    autofocus: false,
                                    keyboardType: TextInputType
                                        .emailAddress, // Use email input type so they have access to special characters
                                    decoration: new InputDecoration(
                                        labelText: 'Password',
                                        contentPadding: EdgeInsets.fromLTRB(
                                            0, 15.0, 0, 15.0),
                                        enabledBorder: UnderlineInputBorder(
                                            borderSide: new BorderSide(
                                                color: Colors.grey))),
                                    validator: validatePassword,
                                    onSaved: (String value) {
                                      this.password = value;
                                    }),
                                new Container(
                                  width: screenSize.width,
                                  child: new RaisedButton(
                                      child: new Text('Login',
                                          style: new TextStyle(
                                              color: Colors.white)),
                                      onPressed: this.submit,
                                      color: Colors.orange,
                                      shape: new RoundedRectangleBorder(
                                          borderRadius:
                                              new BorderRadius.circular(30.0))),
                                  margin: new EdgeInsets.only(top: 20.0),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 15.0),
                                  child: InkWell(
                                    child: Text("Forgot Password?",
                                        style: TextStyle(
                                            decoration:
                                                TextDecoration.underline,
                                            color: Colors.grey)),
                                    onTap: () {
                                      Navigator.pushNamed(
                                          context, '/forgot-password');
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      )),
                  InkWell(
                    child: Text("Click here to sign up!",
                        style: TextStyle(
                            fontSize: 18.0,
                            decoration: TextDecoration.underline,
                            color: Colors.deepOrangeAccent)),
                    onTap: () {
                      _navigateToRegistration(context);
                    },
                  ),
                ])));
  }
}
