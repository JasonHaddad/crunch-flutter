/**
 * 
 * order_list.dart
 * 
 * Jason Haddad
 * 
 * The page which will show all pending orders based on the filters provided (default no filters). Utilized by homepage.dart
 * 
 */

import 'dart:ui';
import 'package:crunch_flutter/repositories/user_repository.dart';
import 'package:crunch_flutter/shared/filter_addition_dialog.dart';
import 'package:crunch_flutter/shared/user_details.dart';
import 'package:crunch_flutter/tracking.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:crunch_flutter/repositories/order_repository.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'models/order.dart';

class OrderListPage extends StatefulWidget {
  @override
  _OrderListPageState createState() => _OrderListPageState();
}

class _OrderListPageState extends State<OrderListPage>
    with AutomaticKeepAliveClientMixin {
  //Used for all order related API calling
  final OrderRepository _orderRepository = new OrderRepository();
  final UserRepository _userRepository = new UserRepository();

  UserDetails userDetails = UserDetails();

  List<GestureDetector> filterChips = new List();
  List<String> filters;
  int nextPage = 0;
  List<GestureDetector> cards = new List<GestureDetector>();

  Future showAcceptedOrdersDialog() async {
    List<Order> orders =
        await _userRepository.getOrdersForRunner(userDetails.user.useremail, 0);

    List<Order> filteredOrders;

    if (orders != null) {
      filteredOrders = orders.where((order) => order.orderstatus == 2).toList();
    }

    showDialog(
        context: context,
        builder: (BuildContext context) {
          if (filteredOrders != null) {
            return AlertDialog(
              content: Container(
                height: 300.0, // Change as per your requirement
                width: 300.0, // Change as per your requirement
                child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: filteredOrders.length,
                  itemBuilder: (BuildContext context, int index) {
                    return ListTile(
                      onTap: () => Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  TrackingPage(filteredOrders[index]))),
                      title: Text(
                        "${filteredOrders[index].eatery.eateryName} - ${filteredOrders[index].orderItems.length.toString()} item(s)",
                        textAlign: TextAlign.left,
                      ),
                    );
                  },
                ),
              ),
            );
          }
          return AlertDialog(content: Text("You haven't accepted any orders!"));
        });
  }

  Future showDeliveredOrdersDialog() async {
    List<Order> orders =
        await _userRepository.getOrdersForRunner(userDetails.user.useremail, 0);

    List<Order> filteredOrders;

    if (orders != null) {
      filteredOrders = orders.where((order) => order.orderstatus == 3).toList();
    }

    showDialog(
        context: context,
        builder: (BuildContext context) {
          if (filteredOrders != null) {
            return AlertDialog(
              content: Container(
                height: 300.0, // Change as per your requirement
                width: 300.0, // Change as per your requirement
                child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: filteredOrders.length,
                  itemBuilder: (BuildContext context, int index) {
                    return ListTile(
                      onTap: () {
                        showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return SimpleDialog(
                                title: Text(
                                    'Order #${filteredOrders[index].orderid}'),
                                children: <Widget>[
                                  SimpleDialogOption(
                                      child: new Column(children: <Widget>[
                                    Text("Items:\n"),
                                    getOrderItems(filteredOrders[index]),
                                    Divider(color: Colors.black),
                                    Text(
                                        "Additional Requests: \n\n${filteredOrders[index].additionalrequests}"),
                                    Divider(color: Colors.black),
                                    Text(
                                        "Cost of order: \$${(filteredOrders[index].ordertotal - 0.75).toStringAsFixed(2)}\n\nWhat you'll earn: \$${(filteredOrders[index].ordertotal * 0.15).toStringAsFixed(2)}"),
                                    Divider(color: Colors.black),
                                    Text(
                                        "Deliver to: ${filteredOrders[index].orderlocation}")
                                  ])),
                                ],
                              );
                            });
                      },
                      title: Text(
                        "${filteredOrders[index].eatery.eateryName} - ${filteredOrders[index].orderItems.length.toString()} item(s)",
                        textAlign: TextAlign.left,
                      ),
                    );
                  },
                ),
              ),
            );
          }
          return AlertDialog(
              content: Text("You haven't delivered any orders!"));
        });
  }

  GestureDetector orderCard(Order order) {
    return GestureDetector(
      onTap: () {
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return SimpleDialog(
                title: Text('Order #${order.orderid}'),
                children: <Widget>[
                  SimpleDialogOption(
                      child: new Column(children: <Widget>[
                    Text("Items:\n"),
                    getOrderItems(order),
                    Divider(color: Colors.black),
                    Text(
                        "Additional Requests: \n\n${order.additionalrequests}"),
                    Divider(color: Colors.black),
                    Text(
                        "Description of user: \n\n${order.customerdescription}"),
                    Divider(color: Colors.black),
                    Text(
                        "Cost of order: \$${(order.ordertotal - 0.75).toStringAsFixed(2)}\n\nWhat you'll earn: \$${(order.ordertotal * 0.15).toStringAsFixed(2)}"),
                    Divider(color: Colors.black),
                    Text("Deliver to: ${order.orderlocation}")
                  ])),
                  SimpleDialogOption(
                    child: new RaisedButton(
                        color: Colors.orange,
                        onPressed: () {
                          _orderRepository.addUserAsRunnerToOrder(order);
                          Navigator.of(context).push(
                            MaterialPageRoute(
                              builder: (BuildContext context) {
                                return TrackingPage(order);
                              },
                            ),
                          );
                        },
                        child: new Text("Accept Order")),
                  ),
                ],
              );
            });
      },
      child: Card(
        elevation: 8.0,
        margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
        child: Stack(
          children: <Widget>[
            Container(
              height: 100,
              decoration: BoxDecoration(
                color: Colors.transparent,
                image: DecorationImage(
                  alignment: Alignment.centerRight,
                  fit: BoxFit.fitHeight,
                  image: AssetImage(
                    "assets/images/" + order.eatery.eateryName + "_logo.jpg",
                  ),
                ),
              ),
            ),
            Container(
              height: 100,
              decoration: BoxDecoration(
                  color: Colors.white,
                  gradient: LinearGradient(
                      begin: FractionalOffset.centerRight,
                      end: FractionalOffset.centerLeft,
                      colors: [
                        Colors.grey[300].withOpacity(0.5),
                        Colors.orange,
                      ],
                      stops: [
                        0.0,
                        1.0
                      ])),
              child: ListTile(
                  contentPadding:
                      EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                  leading: Container(
                    padding: EdgeInsets.only(right: 12.0),
                    decoration: new BoxDecoration(
                        border: new Border(
                            right: new BorderSide(
                                width: 1.0, color: Colors.white24))),
                    child: Icon(Icons.shopping_basket, color: Colors.white),
                  ),
                  title: Text(
                    "${order.eatery.eateryName} - ${order.orderItems.length} items",
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  ),
                  subtitle: Row(
                    children: <Widget>[
                      Icon(Icons.sort, color: Colors.black),
                      Text(" Click for more details",
                          style: TextStyle(color: Colors.white))
                    ],
                  ),
                  trailing: Icon(Icons.keyboard_arrow_right,
                      color: Colors.white, size: 30.0)),
            )
          ],
        ),
      ),
    );
  }

  bool isNumeric(String s) {
    if (s == null) {
      return false;
    }

    // TODO according to DartDoc num.parse() includes both (double.parse and int.parse)
    return double.parse(s, (e) => null) != null ||
        int.parse(s, onError: (e) => null) != null;
  }

  //This will create a list of cards that contain all orders.
  //TODO: This needs to be paginated in the future for sure, or at least not loaded in its entirety right away
  Widget allOrders(List<Order> orders) {
    orders = orders
        .where((order) => order.user.useremail != userDetails.user.useremail)
        .toList();
    if (orders.length > 0) {
      if (filters != null) {
        switch (filters.length) {
          case 2:
            orders = orders
                .where((order) =>
                    order.eatery.eateryName == filters[0] &&
                    order.orderItems.length == int.parse(filters[1]))
                .toList();
            break;
          case 1:
            if (isNumeric(filters[0])) {
              orders = orders
                  .where((order) =>
                      order.orderItems.length == int.parse(filters[0]))
                  .toList();
            } else {
              orders = orders
                  .where((order) => order.eatery.eateryName == filters[0])
                  .toList();
            }
        }
      }
      orders.forEach((order) => cards.add(orderCard(order)));
      return new Container(
        child: new ListView.builder(
            itemCount: cards.length,
            itemBuilder: (BuildContext context, int index) {
              return cards[index];
            }),
      );
    } else if (orders.length == 0 || cards.length == 0) {
      return Padding(
        padding: const EdgeInsets.all(15.0),
        child: Text(
          "There are no orders to show :(",
          style: TextStyle(fontSize: 21),
        ),
      );
    }
  }

  //This function returns the given order's items in a column (for the modal that pops up when the order is clicked)
  Widget getOrderItems(Order order) {
    List<Text> items = new List<Text>();
    order.orderItems.forEach((orderItem) => items.add(Text(orderItem.itemname +
        (orderItem.size == null ? "" : " - " + orderItem.size))));
    return Column(
      children: items,
    );
  }

  //This function creates a little badge that contains an applied filter, to be attached to the blue (for now) filter bar
  GestureDetector filterChip(String chipText) {
    var k = UniqueKey();
    return GestureDetector(
      onTap: () {
        setState(() {
          filters.removeWhere((filterText) => chipText == filterText);
          filterChips.removeWhere((chip) => chip.key == k);
          cards?.clear();
        });
      },
      key: k,
      child: Chip(
        backgroundColor: Colors.pinkAccent,
        padding: EdgeInsets.all(0),
        avatar: CircleAvatar(
            backgroundColor: Colors.black,
            child: Text(
              'X',
              style: TextStyle(color: Colors.white),
            )),
        label: Text(chipText, style: TextStyle(color: Colors.white)),
      ),
    );
  }

  //This is the container that will allow us to add a filter and shows the filters we already have
  Container addFilter() {
    return Container(
      decoration: BoxDecoration(color: Colors.lightBlueAccent),
      child: ListTile(
          contentPadding:
              EdgeInsets.symmetric(horizontal: 20.0, vertical: 40.0),
          title: Text(
            "Add filters",
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
          ),
          subtitle: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: filterChips ?? <Widget>[],
          ),
          trailing: Icon(Icons.add_circle, color: Colors.black, size: 30.0)),
    );
  }

  @override
  Widget build(BuildContext context) {
    cards.clear();
    Future<List<Order>> orders = _orderRepository.getOrders(nextPage);
    return Scaffold(
      body: Column(
        children: <Widget>[
          GestureDetector(
            onTap: () async {
              List filters = await showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return FilterAdditionDialog();
                  });
              setState(() {
                nextPage = 0;
                cards.clear();
                filters
                    ?.forEach((filter) => filterChips.add(filterChip(filter)));
                filters != null ? this.filters = filters : null;
              });
            },
            child: Card(
              margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
              child: addFilter(),
            ),
          ),

          //This will display a spinning circle icon until we get our orders
          FutureBuilder(
              future: orders,

              //set as AsyncSnapshot because then we can access the actual order list with autocomplete if we need to
              builder: (context, AsyncSnapshot<List<Order>> snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  if (snapshot.hasError) {
                    return new Center(
                        child: Text(
                            "Error receiving order list. Please refresh the page"));
                  }
                  //snapshot.data.length < 10 ? null : nextPage++;
                  return Expanded(child: allOrders(snapshot.data));
                } else if (nextPage == 0) {
                  return new Center(
                      child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation<Color>(Colors.orange),
                  ));
                }
              })
        ],
      ),
      floatingActionButton: SpeedDial(
        // both default to 16
        marginRight: 18,
        marginBottom: 20,
        animatedIcon: AnimatedIcons.menu_close,
        animatedIconTheme: IconThemeData(size: 22.0),
        // this is ignored if animatedIcon is non null
        // child: Icon(Icons.add),
        visible: true,
        // If true user is forced to close dial manually
        // by tapping main button and overlay is not rendered.
        closeManually: false,
        curve: Curves.bounceIn,
        overlayColor: Colors.black,
        overlayOpacity: 0.5,
        tooltip: 'Additional Options',
        heroTag: 'speed-dial-hero-tag',
        backgroundColor: Colors.blue,
        foregroundColor: Colors.black,
        elevation: 8.0,
        shape: CircleBorder(),
        children: [
          SpeedDialChild(
              child: Icon(Icons.refresh),
              backgroundColor: Colors.purple,
              label: 'Refresh order list',
              labelStyle: TextStyle(fontSize: 18.0, color: Colors.black),
              onTap: () => setState(() {
                    cards?.clear();
                    filters?.clear();
                    filterChips?.clear();
                  })),
          SpeedDialChild(
            child: Icon(Icons.check_box),
            backgroundColor: Colors.green,
            label: 'Orders you\'ve delivered',
            labelStyle: TextStyle(fontSize: 18.0, color: Colors.black),
            onTap: () => showDeliveredOrdersDialog(),
          ),
          SpeedDialChild(
            child: Icon(Icons.list),
            backgroundColor: Colors.blue,
            label: 'Orders you\'ve accepted',
            labelStyle: TextStyle(fontSize: 18.0, color: Colors.black),
            onTap: () => showAcceptedOrdersDialog(),
          ),
        ],
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
