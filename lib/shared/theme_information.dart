/**
 * 
 * theme_information.dart
 * 
 * Jason Haddad
 * 
 * This will house each college's theme until we store in DB as well, though not sure if that's needed
 * 
 */

import 'package:flutter/material.dart';

class ThemeInformation {
  static const SheridanCollege = {
    'primaryColor': Color.fromRGBO(0, 48, 86, 1.0),
    'accentColor': Color.fromRGBO(0, 159, 189, 1.0),
  };

  static const HumberCollege = {
    'primaryColor': Color.fromRGBO(58, 72, 86, 1.0),
    'accentColor': Color.fromRGBO(0, 109, 156, 1.0),
  };

  static const SenecaCollege = {
    'primaryColor': Color.fromRGBO(218, 41, 28, 1.0),
    'accentColor': Color.fromRGBO(85, 88, 90, 1.0),
  };

  static ThemeData getTheme(Map<String, dynamic> school) {
    return ThemeData(
      brightness: Brightness.dark,
      primaryColor: school['primaryColor'],
      accentColor: school['accentColor'],
    );
  }
}
