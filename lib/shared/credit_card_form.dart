import 'dart:convert';

import 'package:crunch_flutter/models/creditcart.dart';
import 'package:crunch_flutter/repositories/creditcard_repository.dart';
import 'package:crunch_flutter/shared/credit_card_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class CreditCardForm extends StatefulWidget {
  const CreditCardForm({
    Key key,
    this.cardNumber,
    this.expiryDate,
    this.cardHolderName,
    this.cvvCode,
    @required this.resetState,
    this.themeColor,
    this.textColor = Colors.black,
    this.cursorColor,
  }) : super(key: key);

  final String cardNumber;
  final String expiryDate;
  final String cardHolderName;
  final String cvvCode;
  final Function resetState;
  final Color themeColor;
  final Color textColor;
  final Color cursorColor;

  @override
  _CreditCardFormState createState() => _CreditCardFormState();
}

class _CreditCardFormState extends State<CreditCardForm> {
  void setCreditCardModel(CreditCard creditCardModel) {
    cardNumber = creditCardModel.cardnumber;
    expiryDate = creditCardModel.cardexpiry;
    cardHolderName = creditCardModel.cardHolderName;
    cvvCode = creditCardModel.cardccv;
    isCvvFocused = creditCardModel.isCvvFocused;
  }

  int cardNumber;
  String expiryDate;
  String cardHolderName;
  int cvvCode;
  bool isCvvFocused = false;
  Color themeColor;

  CreditCardRepository _creditCardRepository = new CreditCardRepository();
  CreditCard creditCardModel;

  final MaskedTextController _cardNumberController =
      MaskedTextController(mask: '00000');
  final TextEditingController _expiryDateController =
      MaskedTextController(mask: '00/00');
  final TextEditingController _cardHolderNameController =
      TextEditingController();
  final TextEditingController _cvvCodeController =
      MaskedTextController(mask: '000');

  FocusNode cvvFocusNode = FocusNode();

  void createCreditCardModel() {
    cardNumber = widget.cardNumber ?? 0;
    expiryDate = widget.expiryDate ?? '';
    cardHolderName = widget.cardHolderName ?? '';
    cvvCode = widget.cvvCode ?? 0;

    creditCardModel = CreditCard(
        cardNumber, expiryDate, cardHolderName, cvvCode, isCvvFocused);
  }

  @override
  void initState() {
    super.initState();

    createCreditCardModel();

    _cardNumberController.addListener(() {
      cardNumber = int.parse(_cardNumberController.text);
      creditCardModel.cardnumber = cardNumber;
      //resetState();
    });

    _expiryDateController.addListener(() {
      expiryDate = _expiryDateController.text;
      creditCardModel.cardexpiry = expiryDate;
      //resetState();
    });

    _cardHolderNameController.addListener(() {
      cardHolderName = _cardHolderNameController.text;
      creditCardModel.cardHolderName = cardHolderName;
      // resetState();
    });

    _cvvCodeController.addListener(() {
      cvvCode = int.parse(_cvvCodeController.text);
      creditCardModel.cardccv = cvvCode;
      //resetState();
    });
  }

  @override
  void didChangeDependencies() {
    themeColor = widget.themeColor ?? Theme.of(context).primaryColor;
    super.didChangeDependencies();
  }

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        primaryColor: themeColor.withOpacity(0.8),
        primaryColorDark: themeColor,
      ),
      child: Form(
        key: _formKey,
        child: Column(
          children: <Widget>[
            Container(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              margin: const EdgeInsets.only(left: 16, top: 16, right: 16),
              child: TextFormField(
                validator: (value) {
                  if (value.length != 5) {
                    return 'Must be five numbers';
                  }
                  return null;
                },
                controller: _cardNumberController,
                cursorColor: widget.cursorColor ?? themeColor,
                style: TextStyle(
                  color: widget.textColor,
                ),
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Card Number',
                  hintText: 'xxxxx',
                ),
                keyboardType: TextInputType.number,
                textInputAction: TextInputAction.next,
              ),
            ),
            Container(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              margin: const EdgeInsets.only(left: 16, top: 8, right: 16),
              child: TextFormField(
                validator: (value) {
                  if (value.length != 5) {
                    return 'Must enter a date';
                  }
                  return null;
                },
                controller: _expiryDateController,
                cursorColor: widget.cursorColor ?? themeColor,
                style: TextStyle(
                  color: widget.textColor,
                ),
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Expiry Date',
                    hintText: 'MM/YY'),
                keyboardType: TextInputType.number,
                textInputAction: TextInputAction.next,
              ),
            ),
            Container(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              margin: const EdgeInsets.only(left: 16, top: 8, right: 16),
              child: TextFormField(
                focusNode: cvvFocusNode,
                controller: _cvvCodeController,
                cursorColor: widget.cursorColor ?? themeColor,
                validator: (value) {
                  if (value.length != 3) {
                    return 'Must be three numbers';
                  }
                  return null;
                },
                style: TextStyle(
                  color: widget.textColor,
                ),
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'CVV',
                  hintText: 'XXX',
                ),
                keyboardType: TextInputType.number,
                textInputAction: TextInputAction.done,
                onChanged: (String text) {
                  setState(() {
                    cvvCode = int.parse(text);
                  });
                },
              ),
            ),
            // Container(
            //   padding: const EdgeInsets.symmetric(vertical: 8.0),
            //   margin: const EdgeInsets.only(left: 16, top: 8, right: 16),
            //   child: TextFormField(
            //     validator: (value) {
            //       if (value.isEmpty) {
            //         return 'Must enter a name';
            //       }
            //       return null;
            //     },
            //     controller: _cardHolderNameController,
            //     cursorColor: widget.cursorColor ?? themeColor,
            //     style: TextStyle(
            //       color: widget.textColor,
            //     ),
            //     decoration: InputDecoration(
            //       border: OutlineInputBorder(),
            //       labelText: 'Card Holder',
            //     ),
            //     keyboardType: TextInputType.text,
            //     textInputAction: TextInputAction.next,
            //   ),
            // ),
            RaisedButton(
              onPressed: () {
                // Validate returns true if the form is valid, otherwise false.
                if (_formKey.currentState.validate()) {
                  setCreditCardModel(creditCardModel);
                  var cardExpiryConverted = new DateTime(2022, 9, 7, 17);
                  creditCardModel.cardexpiry = cardExpiryConverted.toString();
                  _creditCardRepository.addCard(jsonEncode(creditCardModel));
                  widget.resetState();
                }
              },
              child: Text('Add'),
            )
          ],
        ),
      ),
    );
  }
}
