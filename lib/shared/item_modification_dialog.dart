/**
 * 
 * item_modification_dialog.dart
 * 
 * Jason Haddad
 * 
 * This modal pops up when a user clicks on item on the eatery page. It will lsit all modifications that has
 * and allow the user to select them
 * 
 */

import 'package:crunch_flutter/models/item.dart';
import 'package:crunch_flutter/models/orderitem.dart';
import 'package:flutter/material.dart';
import 'package:grouped_buttons/grouped_buttons.dart';

class ItemModificationDialog extends StatefulWidget {
  final String title;
  final Item item;

  ItemModificationDialog({
    @required this.title,
    @required this.item,
  });

  @override
  _ItemModificationDialogState createState() =>
      new _ItemModificationDialogState();
}

class _ItemModificationDialogState extends State<ItemModificationDialog> {
  //This will be returned when modal closes
  OrderItem itemToAdd;
  TextEditingController additionalRequestsController = TextEditingController();
  String _picked;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    additionalRequestsController.dispose();
    super.dispose();
  }

  //This function loops through an items modifications and lists them
  Widget modificationParameters(Item item) {
    if (item.itemModifications
            .where((mod) => mod.size != "" && mod.size != null)
            .toList()
            .length >
        0) {
      item.itemModifications.forEach((mod) => mod.itemname = item.itemname);
      List<String> sizes = [];
      item.itemModifications
          .forEach((mod) => mod.size != "" ? sizes.add(mod.size) : "");
      _picked == null ? _picked = sizes[0] : null;
      return Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 8, 0, 0),
            child: Text("Select a size", style: TextStyle(fontSize: 15.0)),
          ),
          Container(
              child: RadioButtonGroup(
                  //picked: _picked,
                  labels: sizes,
                  //This is where it 'links' radiogroup to item_modification
                  onSelected: (String selected) {
                    OrderItem filteredItem = item.itemModifications
                        .singleWhere((mod) => mod.size == selected);
                    OrderItem orderItem = new OrderItem(
                        itemid: filteredItem.itemid,
                        itemprice: filteredItem.itemprice,
                        size: filteredItem.size,
                        id: filteredItem.id,
                        itemname: filteredItem.itemname);
                    itemToAdd = orderItem;
                    setState(() {
                      //_picked = selected;
                    });
                  })),
          Text("Price: \$${itemToAdd?.itemprice?.toStringAsFixed(2) ?? "-"}")
        ],
      );
    } else {
      //This is only called if there are no modifications. Ugly, but last minute thing before presentation
      return Text(
        "This item cannot be modified\n\nPrice: \$${widget.item.itemModifications[0].itemprice?.toStringAsFixed(2) ?? "-"}",
        textAlign: TextAlign.center,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
      child: Container(
        width: MediaQuery.of(context).size.width * 0.8,
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 16.0),
                    child: Text(widget.title, style: TextStyle(fontSize: 20.0)),
                  ),
                  Divider(color: Colors.black),
                  modificationParameters(widget.item),
                  Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: TextField(
                      maxLength: 1995,
                      maxLengthEnforced: true,
                      maxLines: 3,
                      controller: additionalRequestsController,
                      decoration: InputDecoration(
                          border: new OutlineInputBorder(
                              borderSide: new BorderSide(color: Colors.black)),
                          hintText: 'Additional requests'),
                      keyboardType: TextInputType.multiline,
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.all(2.0),
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      RaisedButton(
                        color: Colors.grey,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8.0)),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: Text(
                          'Cancel',
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                      RaisedButton(
                        color: Colors.green,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8.0)),
                        onPressed: () {
                          //This either means the user has not selected a size OR it's a non-modifiable item
                          if (itemToAdd == null) {
                            //This means the user hasn't selected an size from the multiple sizes available
                            if (widget.item.itemModifications.length > 1) {
                              showDialog(
                                barrierDismissible: false,
                                context: context,
                                builder: (BuildContext context) {
                                  // return object of type Dialog
                                  return AlertDialog(
                                    title: new Text("Whoops!"),
                                    content: new Text(
                                        "Please select a size for your item."),
                                    actions: <Widget>[
                                      // usually buttons at the bottom of the dialog
                                      new FlatButton(
                                        child: new Text("Got it"),
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                        },
                                      ),
                                    ],
                                  );
                                },
                              );
                            }

                            //This means it's a non-modifiable item because it only has one modification, the default
                            else {
                              itemToAdd = new OrderItem(
                                  itemid: widget.item.itemid,
                                  itemprice: widget
                                      .item.itemModifications[0].itemprice,
                                  size: null,
                                  id: widget.item.itemid,
                                  itemname: widget.item.itemname);
                            }
                          }

                          itemToAdd.additionalrequests =
                              additionalRequestsController.text.isNotEmpty
                                  ? additionalRequestsController.text
                                  : " No additional requests";
                          Navigator.pop(context, itemToAdd);
                        },
                        child: Text(
                          'Add',
                          style: TextStyle(color: Colors.white),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
