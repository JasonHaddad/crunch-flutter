/**
 * 
 * validate_extension.dart
 * 
 * Julian Mooken
 * 
 */

import 'package:validate/validate.dart';
import 'package:phone_number/phone_number.dart';

class ValidateExtension extends Validate {
  static const String PATTERN_LETTERS = "[a-zA-ZöäüÖÄÜß]+\$";
  static const String PATTERN_PHONE= "^(([0-9]{3}))?[-. ]?([0-9]{3})[-. ]?([0-9]{4})\$";
  static const String PATTERN_PASSWORD= "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}\$";

  static void isLetters(String input,
      [String message = Validate.DEFAULT_MATCHES_PATTERN_EX]) {
    Validate.matchesPattern(input, new RegExp(PATTERN_LETTERS), message);
  }

  // static dynamic isPhone(String input) async {
  //   final parsed = await PhoneNumber.format(input, "CA");
  //   return parsed;
  // }

  static void isPhoneNumber(String input,
      [String message = Validate.DEFAULT_MATCHES_PATTERN_EX]) {
    Validate.matchesPattern(input, new RegExp(PATTERN_PHONE), message);
  }

  static void isPassword(String input,
      [String message = Validate.DEFAULT_MATCHES_PATTERN_EX]) {
    Validate.matchesPattern(input, new RegExp(PATTERN_PASSWORD), message);
  }

}

