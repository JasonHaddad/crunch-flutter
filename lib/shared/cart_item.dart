/**
 * 
 * cart_item.dart
 * 
 * Jason Haddad
 * 
 * This is the class for items that comprise the cart
 * 
 */

import 'package:crunch_flutter/models/orderitem.dart';
import 'package:crunch_flutter/shared/item_modification_dialog.dart';
import 'package:flutter/material.dart';

class CartItem extends StatelessWidget {
  OrderItem item;

  //This callback is used to remove an item from the cart
  Function(OrderItem) callback;

  CartItem(this.item, this.callback);

  void _showItemDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text(item.itemname),
          content: new Text("What would you like to do?"),
          actions: <Widget>[
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            // new FlatButton(
            //   child: new Text("Modify", style: TextStyle(color: Colors.green)),
            //   onPressed: () async {
            //     final itemToAdd = await showDialog(
            //         barrierDismissible: false,
            //         context: context,
            //         builder: (BuildContext context) {
            //           return ItemModificationDialog(
            //               title: item.itemname, item: null);
            //         });
            //   },
            // ),
            new FlatButton(
              child: new Text("Delete", style: TextStyle(color: Colors.red)),
              onPressed: () {
                callback(item);
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final itemCard = new Container(
      decoration: new BoxDecoration(
        color: Colors.lightBlue[50],
        shape: BoxShape.rectangle,
        borderRadius: new BorderRadius.circular(8.0),
        boxShadow: <BoxShadow>[
          new BoxShadow(
              color: Colors.grey,
              blurRadius: 10.0,
              offset: new Offset(0.0, 10.0))
        ],
      ),
      child: new Container(
        margin: const EdgeInsets.only(top: 16.0, left: 16.0),
        constraints: new BoxConstraints.expand(),
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            new Text(item?.itemname,
                style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.w600)),
            new Text("\$" + item.itemprice.toStringAsFixed(2),
                style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w300)),
            new Container(
                color: const Color(0xFF00C6FF),
                width: 24.0,
                height: 1.0,
                margin: const EdgeInsets.symmetric(vertical: 8.0)),
            new Row(
              children: <Widget>[
                new Icon(Icons.photo_size_select_small, size: 14.0),
                new Text(" " + (item.size == null ? "N/A" : item.size),
                    style:
                        TextStyle(fontSize: 14.0, fontWeight: FontWeight.w300)),
                new Container(width: 24.0),
              ],
            ),
            new Row(
              children: <Widget>[
                new Text("\n"),
                new Icon(Icons.textsms, size: 14.0),
                Expanded(child: new Text(item.additionalrequests)),
              ],
            )
          ],
        ),
      ),
    );

    return new Container(
      height: 120.0,
      margin: const EdgeInsets.only(top: 16.0, bottom: 8.0),
      child: new FlatButton(
        onPressed: () => {_showItemDialog(context)},
        child: new Stack(
          children: <Widget>[
            itemCard,
          ],
        ),
      ),
    );
  }
}
