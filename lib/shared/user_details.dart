/**
 * 
 * user_details.dart
 * 
 * Jason Haddad
 * 
 * This singleton is populated with our user object when we log in OR when the user is already logged in and
 * opens the application
 * 
 */

import 'package:crunch_flutter/models/user.dart';

class UserDetails{

  UserDetails._privateConstructor();

  static final UserDetails _instance = UserDetails._privateConstructor();

  User user;

  factory UserDetails(){
    return _instance;
  }

}