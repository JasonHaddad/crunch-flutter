/**
 * 
 * cart.dart
 * 
 * Jason Haddad
 * Daniel Hope
 * 
 * This page will store all items they've added to their order (before submission). They can delete items from the cart as well
 * 
 */

import 'dart:convert';
import 'package:crunch_flutter/models/eatery.dart';
import 'package:crunch_flutter/models/order.dart';
import 'package:crunch_flutter/models/orderitem.dart';
import 'package:crunch_flutter/repositories/order_repository.dart';
import 'package:crunch_flutter/values/pickup_locations.dart';
import 'package:flutter/material.dart';
import 'cart_item.dart';

class Cart extends StatefulWidget {
  final List<OrderItem> orderItems;
  final int eateryId;

  //The items the order has, as well as which eatery its for (for order submission)
  Cart(this.orderItems, this.eateryId);

  @override
  State<StatefulWidget> createState() => new _CartState();
}

class _CartState extends State<Cart> {
  OrderRepository _orderRepository = new OrderRepository();
  double surcharge = 0.75;
  double totalPrice = 0;

  TextEditingController descriptionController = TextEditingController();

  //Removes the items and rebuilds the widget with the updated list
  //Used as a callback in cart_item
  removeItem(OrderItem item) {
    widget.orderItems.remove(item);
    setState(() {
      totalPrice = 0;
    });
  }

  updateTotal() {
    if (totalPrice == 0) {
      widget.orderItems
          .forEach((orderItem) => totalPrice += orderItem.itemprice);
      totalPrice = totalPrice * 1.13 + 0.75;
    }
  }

  //The dialog that pops up when the top-right checkmark is clicked for order submission
  void showOrderDialog() {
    if (descriptionController.text.length <= 0) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text("Whoops!"),
              content: new Text(
                  "You need to add a visual description of yourself so that the runner can identify you more easily"),
              actions: <Widget>[
                SimpleDialogOption(
                  child: new FlatButton(
                    child: new Text("Got it"),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ),
              ],
            );
          });
    } else {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text("Confirm"),
              content:
                  new Text("Are you sure you would like to submit your order?"),
              actions: <Widget>[
                SimpleDialogOption(
                  child: new FlatButton(
                    child: new Text("Cancel"),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ),
                SimpleDialogOption(
                  child: new FlatButton(
                    child: new Text("Submit",
                        style: TextStyle(color: Colors.green)),
                    onPressed: () {
                      submitOrder(widget.orderItems);
                      //DH - open confirmation (alert dialog)
                      _orderSentAlert(context);
                    },
                  ),
                ),
              ],
            );
          });
    }
  }

  //Function that begins the API call. Will eventually be cleaner as price can be calcualted before hand
  //Makes an order object (with a corresponding eatery) and passes to 'order_repository.dart'
  int submitOrder(List<OrderItem> items) {
    String allAdditionalRequests = "";
    items.forEach((item) {
      allAdditionalRequests +=
          "${item.itemname} - ${item.additionalrequests}\n";
    });
    String ordertimestamp = DateTime.now().toString();
    Order order = new Order(
        orderid: 0,
        ordertotal: totalPrice,
        ordersurcharge: 0.75,
        ordertimestamp: ordertimestamp,
        orderlocation: eateryLocation,
        eatery: new Eatery(eateryid: widget.eateryId),
        orderItems: items,
        customerdescription: descriptionController.text,
        orderstatus: 1,
        additionalrequests: allAdditionalRequests);
    _orderRepository.createOrder(jsonEncode(order));
    return 1;
  }

  //Dan Hope
  //Modal that pops up upon completion of order
  Future<void> _orderSentAlert(BuildContext context) {
    Icon checkMark = new Icon(
      Icons.check_circle,
      color: Colors.green,
      size: 30.0,
    );
    return showDialog<void>(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Order Complete'),
          content: const Text('Your order has been sent!'),
          actions: <Widget>[
            checkMark,
            RaisedButton(
              color: Colors.orange,
              textColor: Colors.white,
              child: Text('Home Page'),
              onPressed: () {
                Navigator.pushNamedAndRemoveUntil(
                    context, "/home", (_) => false);
              },
            ),
          ],
        );
      },
    );
  }

/**
 * Julian Mooken
 * - Added in eateryLocation selections
 */
  String eateryLocation = "Commons";
  @override
  Widget build(BuildContext context) {
    updateTotal();
    return new Scaffold(
      resizeToAvoidBottomInset: false,
      body: Column(
        children: <Widget>[
          if (widget.orderItems.length > 0)
            new Container(
              padding: new EdgeInsets.all(15.0),
              child: new Text(
                'Please select a pickup location',
              ),
            ),
          if (widget.orderItems.length > 0)
            new DropdownButton<String>(
              value: eateryLocation,
              icon: Icon(Icons.arrow_downward),
              iconSize: 24,
              elevation: 16,
              style: TextStyle(color: Colors.deepPurple),
              underline: Container(
                height: 2,
                color: Colors.deepPurpleAccent,
              ),
              onChanged: (String newValue) {
                setState(() {
                  eateryLocation = newValue;
                });
              },
              items: <String>[
                PickupLocations.sheridanTrafalgarWingsLat.keys.toList()[0],
                PickupLocations.sheridanTrafalgarWingsLat.keys.toList()[1],
                PickupLocations.sheridanTrafalgarWingsLat.keys.toList()[2],
                PickupLocations.sheridanTrafalgarWingsLat.keys.toList()[3],
                PickupLocations.sheridanTrafalgarWingsLat.keys.toList()[4],
                PickupLocations.sheridanTrafalgarWingsLat.keys.toList()[5],
                PickupLocations.sheridanTrafalgarWingsLat.keys.toList()[6],
                PickupLocations.sheridanTrafalgarWingsLat.keys.toList()[7],
                PickupLocations.sheridanTrafalgarWingsLat.keys.toList()[8],
                PickupLocations.sheridanTrafalgarWingsLat.keys.toList()[9]
              ].map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(value),
                );
              }).toList(),
            ),
          if (widget.orderItems.length > 0)
            new Padding(
                child: TextField(
                  maxLength: 195,
                  maxLengthEnforced: true,
                  maxLines: 2,
                  controller: descriptionController,
                  decoration: InputDecoration(
                      border: new OutlineInputBorder(
                          borderSide: new BorderSide(color: Colors.black)),
                      hintText: 'Description of how you look'),
                  keyboardType: TextInputType.multiline,
                ),
                padding: EdgeInsets.all(16.0)),
          Padding(
            padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
            child: Divider(color: Colors.black),
          ),
          Expanded(
            child: new ListView.builder(
                shrinkWrap: true,
                itemExtent: 160.0,
                itemCount: widget.orderItems.length,
                itemBuilder: (_, index) =>
                    CartItem(widget.orderItems[index], removeItem)),
          ),
          if (widget.orderItems.length > 0)
            Padding(
              padding: EdgeInsets.all(15.0),
              child: new Text(
                  "Surcharge: \$$surcharge\n\nTotal: \$${totalPrice.toStringAsFixed(2)} (HST included)",
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 25)),
            ),
          if (widget.orderItems.length == 0)
            new Padding(
              child: new Text(
                "Looks like you have no items in your cart :(",
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 27),
              ),
              padding: new EdgeInsets.all(15.0),
            )
        ],
      ),
      appBar: AppBar(
        elevation: 0.1,
        title: Text("Cart"),
        actions: <Widget>[
          if (widget.orderItems.length > 0)
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 0, 16.0, 0),
              child: GestureDetector(
                  child: Icon(Icons.check_circle),
                  onTap: () {
                    showOrderDialog();
                  }),
            )
        ],
      ),
    );
  }
}
