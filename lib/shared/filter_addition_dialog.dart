/**
 * 
 * filter_addition_dialog.dart
 * 
 * Jason Haddad
 * 
 * This modal pops up when a user clicks on the 'Add Filter' box on the order delivery page.
 * It allows users to filter the list of orders.
 * 
 */

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class FilterAdditionDialog extends StatefulWidget {
  @override
  _FilterAdditionDialogState createState() => new _FilterAdditionDialogState();
}

class _FilterAdditionDialogState extends State<FilterAdditionDialog> {
  //This will be returned when modal closes
  String _selectedEateryFilter;
  TextEditingController number_of_items = TextEditingController();

  @override
  void dispose() {
    //additionalRequestsController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
      backgroundColor: Colors.white,
      title: Text("Add filters"),
      children: <Widget>[
        SimpleDialogOption(
            child: new DropdownButton<String>(
          value: _selectedEateryFilter,
          onChanged: (newValue) {
            setState(() {
              _selectedEateryFilter = newValue;
            });
          },
          hint: Text("Eatery"),
          icon: Icon(Icons.arrow_drop_down),
          items: <String>[
            'Tom Hortons',
            'Ty Express',
            //'Carvey\'s',
            'Third Cup',
            //'Playa\'s Pizza',
            //'Subward'
          ].map((String value) {
            return new DropdownMenuItem<String>(
              value: value,
              child: new Text(value),
            );
          }).toList(),
        )),
        // SimpleDialogOption(
        //     child: TextField(
        //   decoration: new InputDecoration(
        //       icon: Icon(Icons.location_searching),
        //       hintText: 'Distance',
        //       border: InputBorder.none),
        // )),
        // SimpleDialogOption(
        //     child: TextField(
        //   controller: number_of_items,
        //   keyboardType: TextInputType.number,
        //   inputFormatters: <TextInputFormatter>[
        //     WhitelistingTextInputFormatter.digitsOnly
        //   ],
        //   decoration: new InputDecoration(
        //       icon: Icon(Icons.fastfood),
        //       hintText: 'Number of Items',
        //       border: InputBorder.none),
        // )),
        SimpleDialogOption(
          child: new RaisedButton(
              color: Colors.orange,
              onPressed: () {
                //TODO: Logic works, but is absolutely funky, will fix
                if (_selectedEateryFilter != null) {
                  if (number_of_items.text != "") {
                    Navigator.pop(
                        context, [_selectedEateryFilter, number_of_items.text]);
                  }
                  Navigator.pop(context, [_selectedEateryFilter]);
                } else if (number_of_items.text != "") {
                  Navigator.pop(context, [number_of_items.text]);
                } else {
                  Fluttertoast.showToast(
                      msg: "You haven't added any filters!",
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.BOTTOM,
                      timeInSecForIos: 2,
                      backgroundColor: Colors.grey,
                      textColor: Colors.white,
                      fontSize: 16.0);
                }
              },
              child: new Text("Add")),
        ),
        SimpleDialogOption(
          child: new RaisedButton(
              color: Colors.orange,
              onPressed: () {
                Navigator.pop(context);
              },
              child: new Text("Cancel")),
        )
      ],
    );
  }
}
