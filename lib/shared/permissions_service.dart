/**
 * 
 * permissions_service.dart
 * 
 * Julian Mooken
 * 
 * Where all permissions for application are handled. So far, only location permissions are needed
 * 
 */

import 'package:permission_handler/permission_handler.dart';

class PermissionsService {
  final PermissionHandler _permissionHandler = PermissionHandler();

   Future<bool> _requestPermission(PermissionGroup permission) async {
    var result = await _permissionHandler.requestPermissions([permission]);
    if (result[permission] == PermissionStatus.granted) {
      return true;
    }
    return false;
  }

  // /// Requests the users permission to read their contacts.
  // Future<bool> requestContactsPermission() async {
  //   return _requestPermission(PermissionGroup.contacts);
  // }

  /// Requests the users permission to read their location when the app is in use
  Future<bool> requestLocationPermission({Function onPermissionDenied}) async {
    var granted = await _requestPermission(PermissionGroup.locationWhenInUse);
    if (!granted) {
      onPermissionDenied();
    }
    return granted;
  }

  // Future<bool> hasContactsPermission() async {
  //   return hasPermission(PermissionGroup.contacts);
  // }

  Future<bool> hasPermission(PermissionGroup permission) async {
    var permissionStatus =
        await _permissionHandler.checkPermissionStatus(permission);
    return permissionStatus == PermissionStatus.granted;
  }
  
}