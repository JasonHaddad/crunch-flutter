/**
 * 
 * app_drawer.dart
 * 
 * Jason Haddad
 * Daniel Hope
 * 
 * This class is the persistent drawer that exists throughout the application.
 * 
 */

import 'package:crunch_flutter/homepage.dart';
import 'package:crunch_flutter/my_orders.dart';
import 'package:crunch_flutter/shared/user_details.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';

import 'package:crunch_flutter/profile.dart';
import 'package:crunch_flutter/settings.dart';
import 'package:url_launcher/url_launcher.dart';

class AppDrawer extends StatelessWidget {
  _launchURL(String toMailId, String subject, String body) async {
    var url = 'mailto:$toMailId?subject=$subject&body=$body';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  //Singleton of our user details
  UserDetails userDetails = UserDetails();

  @override
  Widget build(BuildContext context) {
    return Drawer(
      elevation: 20.0,
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          UserAccountsDrawerHeader(
            accountName: Text(
                "${userDetails.user.userfirstname} ${userDetails.user.userlastname}"),
            accountEmail: Text(userDetails.user.useremail),
            currentAccountPicture: Image.asset("assets/images/logo.png"),
            // onDetailsPressed: () {
            //   Navigator.of(context).pop();
            //   Navigator.of(context).push(MaterialPageRoute(
            //       builder: (BuildContext context) => MyProfilePage()));
            // },
          ),
          // ListTile(
          //   title: Text("Home"),
          //   trailing: Icon(Icons.home),
          //   onTap: () {
          //     Navigator.of(context).pop();
          //     Navigator.of(context).push(MaterialPageRoute(
          //         builder: (BuildContext context) => MyHomePage()));
          //   },
          // ),
          ListTile(
            title: Text("My Orders"),
            trailing: Icon(Icons.list),
            onTap: () {
              Navigator.of(context).pop();
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (BuildContext context) => MyOrders()));
            },
          ),
          ListTile(
            title: Text("Settings"),
            trailing: Icon(Icons.settings),
            onTap: () {
              Navigator.of(context).pop();
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (BuildContext context) => Settings()));
            },
          ),
          ListTile(
            title: Text("Crunch Support"),
            trailing: Icon(Icons.live_help),
            // prepopulate a generic support email for ease of use
            onTap: () {
              _launchURL('crunch.capstone@gmail.com', 'Crunch Support',
                  'Please let us know how we can help!\n\n');
            },
          )
        ],
      ),
    );
  }
}
