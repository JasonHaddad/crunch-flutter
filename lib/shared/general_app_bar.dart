/**
 * 
 * general_app_bar.dart
 * 
 * Jason Haddad
 * 
 * Common app bar throughout application that just lists title of page
 * 
 */

import 'package:flutter/material.dart';

class GeneralAppBar {
  final String title;
  const GeneralAppBar(this.title);

  Widget setAppBar() {
    return AppBar(
        elevation: 0.1,
        title: Text(title));
  }
}
