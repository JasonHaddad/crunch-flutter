/**
 * 
 * homepage.dart
 * 
 * Jason Haddad
 * 
 * The initial landing page for the application (After logging in and/or when running the app while already being logged in)
 * From here you can create an order, choose an order to deliver, and more
 * 
 */
import 'package:badges/badges.dart';
import 'package:crunch_flutter/order_list.dart';
import 'package:crunch_flutter/create_order.dart';
import 'package:crunch_flutter/repositories/order_repository.dart';
import 'package:crunch_flutter/repositories/user_repository.dart';
import 'package:crunch_flutter/shared/user_details.dart';
import 'package:crunch_flutter/tracking.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'models/order.dart';
import 'shared/app_drawer.dart';

class MyHomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage>
    with AutomaticKeepAliveClientMixin {
  //Used to get a user's last order
  UserRepository _userRepository = new UserRepository();
  OrderRepository _orderRepository = new OrderRepository();
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  List<PopupMenuEntry<Order>> currentOrderList = new List();
  List<Order> currentUserOrders;

  Widget currentOrders() => PopupMenuButton<Order>(
        icon: Icon(Icons.location_on),
        onSelected: (value) {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (BuildContext context) {
                return TrackingPage(value);
              },
            ),
          );
        },
        itemBuilder: (context) => currentOrderList,
      );
  @override
  void initState() {
    super.initState();

    _firebaseMessaging.onTokenRefresh.listen(sendTokenToServer);
    _firebaseMessaging.getToken();

    _firebaseMessaging.subscribeToTopic("all");
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        setState(() {
          currentOrderList?.clear();
          currentUserOrders?.clear();
        });
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
        setState(() {
          currentOrderList.clear();
          currentUserOrders.clear();
        });
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume WE GOT AN ORDER: $message");
        setState(() {
          currentOrderList.clear();
          currentUserOrders.clear();
        });
      },
    );
    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(sound: true, badge: true, alert: true));
  }

  //Singleton of our user details
  UserDetails userDetails = UserDetails();

  Widget orderTabContents() {
    return CreateOrder(getLastUserOrderAndAllUserOrders());
  }

  Widget deliveryTabContents() {
    return OrderListPage();
  }

  Widget getOrderItems(Order order) {
    List<Text> items = new List<Text>();
    order.orderItems.forEach((orderItem) => items.add(Text(orderItem.itemname +
        (orderItem?.size == null ? "" : " - " + orderItem.size))));
    return Column(
      children: items,
    );
  }

  void showDeletionDialog(int orderId) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Confirm Deletion"),
            content: Text(
                "Deleting this order will refund you the appropriate amount"),
            actions: [
              FlatButton(
                child: Text("Close"),
                onPressed: () {
                  Navigator.pop(context, false);
                },
              ),
              FlatButton(
                color: Colors.red,
                child: Text("Delete"),
                onPressed: () {
                  //Pop twice to dismiss dialog and reset home page
                  _orderRepository.deleteOrder(orderId);
                  Navigator.pop(context, false);
                  Navigator.pop(context, false);
                  setState(() {});
                },
              )
            ],
          );
        });
  }

  //This is the widget that will display your last order (if it exists, called from lastUserOrder())
  Column ordersExistsInfo(Order order) {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(15.0),
          child: Text("Your most recent order"),
        ),
        GestureDetector(
          onTap: () {
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return SimpleDialog(
                    title: Text('Order #${order.orderid}'),
                    children: <Widget>[
                      SimpleDialogOption(
                          child: new Column(children: <Widget>[
                        Text("Items:\n"),
                        getOrderItems(order),
                        Divider(color: Colors.black),
                        Text(
                            "Additional Requests: \n\n${order.additionalrequests}"),
                        Divider(color: Colors.black),
                        Text(
                            "Description of user: \n\n${order.customerdescription}"),
                        Divider(color: Colors.black),
                        Text(
                            "Cost of order: \$${(order.ordertotal - order.ordersurcharge).toStringAsFixed(2)}"),
                        if (order.orderstatus == 1)
                          FlatButton(
                            child: Text("Cancel Order"),
                            color: Colors.red,
                            onPressed: () {
                              showDeletionDialog(order.orderid);
                            },
                          ),
                        if (order.orderstatus == 2)
                          FlatButton(
                            child: Text("Tracking Page"),
                            color: Colors.green,
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          TrackingPage(order)));
                            },
                          )
                      ])),
                    ],
                  );
                });
          },
          child: Card(
            elevation: 8.0,
            margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
            child: Stack(
              children: <Widget>[
                Container(
                  height: 100,
                  decoration: BoxDecoration(
                    color: Colors.transparent,
                    image: DecorationImage(
                      alignment: Alignment.centerRight,
                      fit: BoxFit.fitHeight,
                      image: AssetImage(
                        "assets/images/" +
                            order.eatery.eateryName +
                            "_logo.jpg",
                      ),
                    ),
                  ),
                ),
                Container(
                  height: 100,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      gradient: LinearGradient(
                          begin: FractionalOffset.centerRight,
                          end: FractionalOffset.centerLeft,
                          colors: [
                            Colors.grey[300].withOpacity(0.5),
                            order.orderstatus == 2
                                ? Colors.orange
                                : (order.orderstatus == 3
                                    ? Colors.green
                                    : Colors.redAccent),
                          ],
                          stops: [
                            0.0,
                            1.0
                          ])),
                  child: ListTile(
                      contentPadding: EdgeInsets.symmetric(
                          horizontal: 20.0, vertical: 10.0),
                      leading: Container(
                        padding: EdgeInsets.only(right: 12.0),
                        decoration: new BoxDecoration(
                            border: new Border(
                                right: new BorderSide(
                                    width: 1.0, color: Colors.white24))),
                        child: order.orderstatus == 2
                            ? Icon(Icons.directions_walk, color: Colors.white)
                            : (order.orderstatus == 3
                                ? Icon(Icons.check_circle, color: Colors.white)
                                : Icon(Icons.hourglass_full,
                                    color: Colors.white)),
                      ),
                      title: Text(
                        "${order.eatery.eateryName} - ${order.orderItems.length} items",
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold),
                      ),
                      subtitle: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(children: <Widget>[
                            Icon(Icons.sort, color: Colors.black),
                            Text(" Click for more details",
                                style: TextStyle(color: Colors.white))
                          ]),
                          Text(
                              order.orderstatus == 2
                                  ? "\nAccepted - Delivery in Progress"
                                  : (order.orderstatus == 3
                                      ? "\nDelivered"
                                      : "\nWaiting for acceptance..."),
                              style: TextStyle(color: Colors.black38),
                              textAlign: TextAlign.left)
                        ],
                      ),
                      trailing: Icon(Icons.keyboard_arrow_right,
                          color: Colors.grey, size: 30.0)),
                )
              ],
            ),
          ),
        )
      ],
    );
  }

  //This will create the orderExistsInfo widget if the user has a 'last order', or text otherwise
  Widget getLastUserOrderAndAllUserOrders() {
    return FutureBuilder(
        future: _userRepository.getOrdersForUser(userDetails.user.useremail, 0),
        //set as AsyncSnapshot because then we can access the actual order list with autocomplete if we need to
        builder: (context, AsyncSnapshot<List<Order>> snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.data == null) {
              return Padding(
                padding: const EdgeInsets.all(15.0),
                child: new Center(
                    child: Text(
                        "You don't have any orders! Why not create one below?")),
              );
            }
            snapshot.data.sort((a, b) => b.orderid.compareTo(a.orderid));
            snapshot.data
                .where((order) => order.orderstatus != 3)
                .forEach((order) => currentOrderList.add(PopupMenuItem<Order>(
                      value: order,
                      child: new Text(
                          '${order.eatery.eateryName} - ${order.orderItems.length} item(s)'),
                    )));
            return ordersExistsInfo(snapshot.data[0]);
          } else {
            return new Center(
                child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(Colors.orange),
            ));
          }
        });
  }

  @override
  Widget build(BuildContext context) {
    currentOrderList?.clear();
    currentUserOrders?.clear();
    return new DefaultTabController(
      length: 2,
      child: Scaffold(
        drawer: AppDrawer(),
        appBar: AppBar(
          actions: <Widget>[
            // Badge(
            //   //badgeColor: currentOrderList != null && currentOrderList.length  > 0 ? Colors.red.withOpacity(1) : Colors.red.withOpacity(0),
            //   //badgeContent: Text(""),
            //   //position: BadgePosition.topLeft(left: 5),
            //   child: currentOrders(),
            // ),
          ],
          bottom: TabBar(
            indicator: BoxDecoration(color: Color.fromRGBO(255, 255, 255, 0.6)),
            tabs: [
              Tab(icon: Icon(Icons.view_list), text: "Order"),
              Tab(icon: Icon(Icons.send), text: "Deliver"),
            ],
          ),
        ),
        body: TabBarView(
          children: [orderTabContents(), deliveryTabContents()],
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;

  void sendTokenToServer(String fcmToken) {
    userDetails.user.userToken = fcmToken;
    _orderRepository.updateUserTokenOnOrder(fcmToken);
  }
}
