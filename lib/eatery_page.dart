/**
 * 
 * eatery_page.dart
 * 
 * Jason Haddad
 * 
 * This page lists all items for the given eatery and separates items by category. Clicked items open up an 'ItemModificationDialog'
 * that allows users to modify their item. Added items are stored in a Cart object (in Cart.dart)
 * 
 */

import 'package:crunch_flutter/shared/Cart.dart';
import 'package:crunch_flutter/shared/item_modification_dialog.dart';
import 'package:crunch_flutter/values/eatery_category_ids.dart';
import 'package:crunch_flutter/values/eatery_tabs.dart';
import 'package:flutter/material.dart';
import 'package:crunch_flutter/repositories/item_repository.dart';
import 'models/item.dart';
import 'models/orderitem.dart';
import 'package:cached_network_image/cached_network_image.dart';

class EateryPage extends StatefulWidget {
  //Passed from create_order.dart; eateryId is used when we getItemsForEatery by eatery id
  final String eatery;
  final int eateryId;
  EateryPage(this.eatery, this.eateryId);

  @override
  _EateryState createState() => _EateryState();
}

class _EateryState extends State<EateryPage>
    with AutomaticKeepAliveClientMixin {
  //Will be used to get items by eatery
  final ItemRepository _itemRepository = new ItemRepository();

  //Stores all items we are ordering/have added to cart
  List<OrderItem> itemsToOrder = new List();

  //Widget that contains a list of tabs for this eatery (based on EateryTabs file)
  List<Tab> eateryTabs(String eatery) {
    List<Tab> tabList = new List();
    EateryTabs.getTabs(eatery)
        .forEach((tab) => tabList.add(Tab(child: Text(tab))));
    return tabList;
  }

  //This returns a widget containing a lsit of items that fall under the given category
  Widget itemsBasedOnCategory(Future<List<Item>> items, int categoryId) {
    return FutureBuilder(
        future: items,
        //set as AsyncSnapshot because then we can access the actual order list with autocomplete if we need to
        builder: (context, AsyncSnapshot<List<Item>> snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasError) {
              return new Center(child: Text("Error receiving items"));
            }

            //This is the actual function/Widget that is displayed for items in a given category
            return showItems(
                snapshot.data
                    .where((item) => item.categoryid == categoryId)
                    .toList(),
                context);
          } else {
            return new Center(
                child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(Colors.orange),
            ));
          }
        });
  }

  /*
    This widget generates a grid of items (that have been previously separated by category).
    Each item has 'onTap' functionality that opens up a modal that allows the user to modify
    the item (size and additional requests only for now)

  */
  Widget showItems(List<Item> items, BuildContext context) {
    var _width = MediaQuery.of(context).size.width / 3;
    return GridView.builder(
      shrinkWrap: true,
      physics: BouncingScrollPhysics(),
      itemCount: items.length,
      gridDelegate:
          SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
      itemBuilder: (context, index) {
        return GestureDetector(
          onTap: () async {
            final itemToAdd = await showDialog(
                barrierDismissible: false,
                context: context,
                builder: (BuildContext context) {
                  return ItemModificationDialog(
                      title: items[index].itemname, item: items[index]);
                });

            //TODO: hopefully we make it so we dont rebuild the whole page just to update variable, because it makes another API call
            //Rebuild the widget in order to update list of items to order
            setState(() {
              //Null will eventually be error handled (if its needed)
              itemToAdd != null ? itemsToOrder.add(itemToAdd) : null;
            });
          },
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: <Widget>[
                Card(
                  elevation: 5,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0),
                  ),
                  child: Container(
                    width: _width,
                    height: _width - 20,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(8.0),
                      child: FadeInImage(
                        //TODO: Fix when we have actual item images
                        image:
                            CachedNetworkImageProvider(items[index].imageLink),
                        fit: BoxFit.cover,
                        placeholder: AssetImage('assets/images/logo.png'),
                      ),
                    ),
                  ),
                ),
                Text(items[index].itemname),
              ],
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    Future<List<Item>> items =
        _itemRepository.getItemsForEatery(widget.eateryId);

    //Creates a page with tabs and items separated by category
    return DefaultTabController(
        length: eateryTabs(widget.eatery).length,
        child: Scaffold(
          appBar: AppBar(
            actions: <Widget>[
              Padding(
                  padding: const EdgeInsets.only(right: 16.0, top: 8.0),
                  child: new Stack(children: <Widget>[
                    IconButton(
                      icon: Icon(Icons.shopping_cart),
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    Cart(itemsToOrder, widget.eateryId)));
                      },
                    ),
                    new Positioned(
                      //To show number of items in cart
                      top: 0.0,
                      right: 0.0,
                      child: new Container(
                        padding: EdgeInsets.all(1),
                        decoration: new BoxDecoration(
                            color: Colors.blue,
                            borderRadius: BorderRadius.circular(6)),
                        child: new Text(
                          '${itemsToOrder.length}',
                          style: new TextStyle(
                            color: Colors.white,
                            fontSize: 8,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    )
                  ]))
            ],
            elevation: 0.1,
            centerTitle: true,
            leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                if (itemsToOrder.length > 0) {
                  showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: Text("Careful!"),
                        content: Text(
                            "If you leave leave this page, all items in your cart will be lost."),
                        actions: [
                          FlatButton(
                            child: Text("Cancel"),
                            onPressed: () {
                              Navigator.pop(context, false);
                            },
                          ),
                          FlatButton(
                            child: Text("Understood"),
                            onPressed: () {
                              //Pop twice to dismiss dialog and head back to home page
                              Navigator.pop(context, false);
                              Navigator.pop(context, false);
                            },
                          )
                        ],
                      );
                    },
                  );
                } else {
                  //Otherwise, just go back
                  Navigator.pop(context, false);
                }
              },
            ),
            title: Text(widget.eatery),
            bottom: PreferredSize(
                child: TabBar(
                    isScrollable: true,
                    tabs: eateryTabs(widget.eatery)),
                preferredSize: Size.fromHeight(45.0)),
          ),
          body: TabBarView(
            children: List<Widget>.generate(
              EateryCategoryIds.getCategoryIds(widget.eatery).length,
              (int index) {
                return itemsBasedOnCategory(items,
                    EateryCategoryIds.getCategoryIds(widget.eatery)[index]);
              },
            ),
          ),
        ));
  }

  @override
  bool get wantKeepAlive => true;
}
