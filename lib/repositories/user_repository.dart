/**
 * 
 * urder_repository.dart
 * 
 * Jason Haddad
 * 
 * Will handle all API functionality for users 
 * 
 */

import 'dart:convert';
import 'package:crunch_flutter/models/order.dart';
import 'package:crunch_flutter/models/user.dart';
import 'package:dbcrypt/dbcrypt.dart';
import 'package:http/http.dart';

final String username = "admin";
final String password = "password";
final credentials = '$username:$password';

class UserRepository {
  Future<User> login(String useremail, String userpassword) async {
    Response response =
        await get('http://18.220.255.180:8080/user/' + useremail, headers: {"Authorization": "Basic ${utf8.fuse(base64).encode(credentials)}"});
    User user = response.body != "null"
        ? User.fromJson(json.decode(response.body))
        : null;
    if (user != null &&  user.userpassword == DBCrypt().hashpw(userpassword, user.usersalt)) {
      return user;
    }

    //Our current 'failed' login result
    return new User();
  }

  Future<int> register(User user) async {
    int statusCode;
    await post('http://18.220.255.180:8080/user',
            body: json.encode(user.toJson()),
            headers: {"Content-type": "application/json", "Authorization": "Basic ${utf8.fuse(base64).encode(credentials)}"})
        .then((Response response) {
      statusCode = response.statusCode;
      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw new Exception(
            "Error while fetching data, status code: " + statusCode.toString());
      }
    });
    return statusCode;
  }

  Future<List<Order>> getOrdersForUser(String useremail, int page) async{
    await get('http://18.220.255.180:8080/cache/clearAll');
    Response response = await get('http://18.220.255.180:8080/user/orders/$useremail?page=$page', headers: {"Authorization": "Basic ${utf8.fuse(base64).encode(credentials)}"});
    Iterable result = json.decode(response.body);
    List<Order> orders = response.body != "[]" ? result.map((i) => Order.fromJson(i)).toList() : null;
    return orders;
  }

  Future<List<Order>> getOrdersForRunner(String useremail, int page) async{
    await get('http://18.220.255.180:8080/cache/clearAll');
    Response response = await get('http://18.220.255.180:8080/user/orders/runner/$useremail?page=$page', headers: {"Authorization": "Basic ${utf8.fuse(base64).encode(credentials)}"});
    Iterable result = json.decode(response.body);
    List<Order> orders = response.body != "[]" ? result.map((i) => Order.fromJson(i)).toList() : null;
    return orders;
  }
}
