/**
 * 
 * creditcard_repository.dart
 * 
 * Daniel Hope
 * 
 * Will handle all API functionality for credit cards 
 * 
 */

import 'package:crunch_flutter/shared/user_details.dart';
import 'package:http/http.dart';
import 'dart:convert';
import 'package:crunch_flutter/models/creditcart.dart';

// final String username = "admin";
// final String password = "password";
// final credentials = '$username:$password';

UserDetails userDetails = UserDetails();

class CreditCardRepository {
  Future<int> addCard(String creditCardJson) async {
    String email = userDetails.user.useremail;
    Response response = await post(
        'http://18.220.255.180:8080/creditcard/addCard/$email',
        body: creditCardJson,
        headers: {
          "Content-type": "application/json",
        });
    return response.statusCode;
  }

  Future<List<CreditCard>> getCards() async {
    String email = userDetails.user.useremail;
    Response response =
        await get('http://18.220.255.180:8080/creditcard/$email', headers: {
      "Content-type": "application/json",
    });
    Iterable result = json.decode(response.body);
    List<CreditCard> cards = result.map((i) => CreditCard.fromJson(i)).toList();
    return cards;
  }
}
