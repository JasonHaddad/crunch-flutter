/**
 * 
 * item_repository.dart
 * 
 * Jason Haddad
 * 
 * Will handle all API functionality for items 
 * 
 */

import 'dart:convert';
import 'package:crunch_flutter/models/item.dart';
import 'package:http/http.dart';

final String username = "admin";
final String password = "password";
final credentials = '$username:$password';

class ItemRepository {
  Future<List<Item>> getItems() async {
    Response response = await get('http://18.220.255.180:8080/item',
        headers: {"Authorization": "Basic ${utf8.fuse(base64).encode(credentials)}"});
    Iterable result = json.decode(response.body);
    List<Item> items = result.map((i) => Item.fromJson(i)).toList();
    return items;
  }

  Future<Item> getItem(int itemId) async {
    Response response = await get('http://18.220.255.180:8080/item/$itemId',
        headers: {"Authorization": "Basic ${utf8.fuse(base64).encode(credentials)}"});
    Item item = Item.fromJson(json.decode(response.body));
    return item;
  }

  Future<List<Item>> getItemsForEatery(int eateryId) async {
    Response response = await get(
        'http://18.220.255.180:8080/item/eatery/' + eateryId.toString(), headers: {"Authorization": "Basic ${utf8.fuse(base64).encode(credentials)}"});
    Iterable result = json.decode(response.body);
    List<Item> items = result.map((i) => Item.fromJson(i)).toList();
    return items;
  }
}
