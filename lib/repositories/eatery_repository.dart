/**
 * 
 * eatery_repository.dart
 * 
 * Jason Haddad
 * 
 * Will eventually be needed when there are multiple colleges, and will handle all API functionality for eateries 
 * 
 */

import 'dart:convert';
import 'package:crunch_flutter/models/eatery.dart';
import 'package:http/http.dart';

final String username = "admin";
final String password = "password";
final credentials = '$username:$password';

class EateryRepository {

  Future<List<Eatery>> getEateries() async{
    Response response = await get('http://18.220.255.180:8080/eatery', headers: {"Authorization": "Basic ${utf8.fuse(base64).encode(credentials)}"});
    Iterable result = json.decode(response.body);
    List<Eatery> eateries = result.map((i) => Eatery.fromJson(i)).toList();
    return eateries;
  }
  
}