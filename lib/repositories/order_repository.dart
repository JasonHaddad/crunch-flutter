/**
 * 
 * order_repository.dart
 * 
 * Jason Haddad
 * 
 * Will handle all API functionality for orders 
 * 
 */

import 'dart:convert';
import 'package:crunch_flutter/models/order.dart';
import 'package:crunch_flutter/repositories/push_notifications.dart';
import 'package:crunch_flutter/shared/user_details.dart';
import 'package:http/http.dart';

final String username = "admin";
final String password = "password";
final credentials = '$username:$password';

UserDetails userDetails = UserDetails();

class OrderRepository {
  Future<List<Order>> getOrders(int page) async {
    await get('http://18.220.255.180:8080/cache/clearAll');
    Response response = await get('http://18.220.255.180:8080/order?page=$page',
        headers: {
          "Authorization": "Basic ${utf8.fuse(base64).encode(credentials)}"
        });
    Iterable result = json.decode(response.body);
    List<Order> orders = result.map((i) => Order.fromJson(i)).toList();

    return orders;
  }

  createOrder(String jsonOrder) async {
    print(jsonOrder);
    await get('http://18.220.255.180:8080/cache/clearAll');
    String email = userDetails.user.useremail;
    String userToken = userDetails.user.userToken;
    Response response = await put(
        'http://18.220.255.180:8080/order/addCustomer/$email/$userToken',
        body: jsonOrder,
        headers: {
          "Content-type": "application/json",
          "Authorization": "Basic ${utf8.fuse(base64).encode(credentials)}"
        });
  }

  addUserAsRunnerToOrder(Order order) async {
    await get('http://18.220.255.180:8080/cache/clearAll');
    String email = userDetails.user.useremail;
    await put(
        'http://18.220.255.180:8080/order/addRunner/${order.orderid}/$email',
        headers: {
          "Authorization": "Basic ${utf8.fuse(base64).encode(credentials)}"
        });
    PushNotifications.sendTo(
        title: "Your order from ${order.eatery.eateryName} has been accepted!",
        body: "",
        fcmToken: order.usertoken);
  }

  updateUserTokenOnOrder(String userToken) async {
    await get('http://18.220.255.180:8080/cache/clearAll');
    String email = userDetails.user.useremail;
    await put('http://18.220.255.180:8080/order/updateToken/$email/$userToken',
        headers: {
          "Authorization": "Basic ${utf8.fuse(base64).encode(credentials)}"
        });
  }

  deleteOrder(int orderId){
    get('http://18.220.255.180:8080/cache/clearAll');
    delete('http://18.220.255.180:8080/order/$orderId');
  }
  
  deliverOrder(int orderId) {
    get('http://18.220.255.180:8080/cache/clearAll');
    put('http://18.220.255.180:8080/order/orderDelivered/$orderId');
  }
}