/**
 * 
 * settings_account.dart
 * 
 * Julian Mooken
 * 
 * This page simply displays all data relvanat to the user about themselves. Will be editable at some point.
 * 
 */

import 'package:crunch_flutter/shared/general_app_bar.dart';
import 'package:crunch_flutter/shared/user_details.dart';
import 'package:flutter/material.dart';

UserDetails userDetails = UserDetails();

class SettingsAccount extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
          child: Container(
            padding: new EdgeInsets.fromLTRB(10, 100, 10, 100),
            child: Card(
              elevation: 3.0,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  CircleAvatar(
                    radius: 50,
                    backgroundImage: AssetImage('assets/images/logo.png'),
                  ),
                  Text(
                    '${userDetails.user.userfirstname} ${userDetails.user.userlastname}',
                    style: TextStyle(
                      fontSize: 40.0,
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Pacifico',
                    ),
                  ),
                  SizedBox(
                    height: 20,
                    width: 200,
                    child: Divider(color: Colors.black),
                  ),
                  InfoCard(
                    text: userDetails.user.userphonenumber,
                    icon: Icons.phone,
                  ),
                  InfoCard(
                    text: userDetails.user.useremail,
                    icon: Icons.email,
                  ),
                  InfoCard(
                    text: 'Sheridan College - TRA',
                    icon: Icons.school,
                  ),
                ],
              ),
            ),
          ),
        ),
        appBar: GeneralAppBar("Account").setAppBar());
  }
}

class InfoCard extends StatelessWidget {
  final String text;
  final IconData icon;
  Function onPressed;

  InfoCard({
    @required this.text,
    @required this.icon,
    this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Card(
        color: Colors.white,
        margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
        child: ListTile(
          leading: Icon(
            icon,
            color: Colors.teal,
          ),
          title: Text(
            text,
            style: TextStyle(
              fontFamily: 'Source Sans Pro',
              fontSize: 20.0,
              color: Colors.teal,
            ),
          ),
        ),
      ),
    );
  }
}
