/**
 * 
 * settings_notifications.dart
 * 
 * Julian Mooken
 * 
 * The page provides users with the ability to turn on/off specific notifactions that the app sends out
 * 
 */

import 'package:crunch_flutter/shared/general_app_bar.dart';
import 'package:flutter/material.dart';

class SettingsNotifications extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _SettingsNotificationsState();
  }
}

class _SettingsNotificationsState extends State<SettingsNotifications> {
  //TODO: these values will be pulled (from db, cache, or sharedprefs) when this page is accessed, defaulting to true each time AT THE MOMENT
  bool orderAcceptedNotifications = true;
  bool newsPromotionsNotifications = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
          children: <Widget>[
            Padding(
                child: Text("The below options toggle push notifications."), padding: EdgeInsets.all(30.0)),
            Card(
              elevation: 1.0,
              margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
              child: Container(
                child: ListTile(
                  contentPadding:
                      EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                  title: Text(
                    "Order Accepted",
                    style: TextStyle(
                        color: Colors.black, fontWeight: FontWeight.bold),
                  ),
                  // subtitle: Text("test", style: TextStyle(color: Colors.white)),
                  trailing: new Switch(
                    value: orderAcceptedNotifications,
                    onChanged: (value) {
                      setState(() {
                        orderAcceptedNotifications = value;
                      });
                    },
                    activeTrackColor: Colors.lightGreenAccent,
                    activeColor: Colors.green,
                    inactiveTrackColor: Colors.grey,
                    inactiveThumbColor: Colors.grey,
                  ),
                ),
              ),
            ),
            Card(
              elevation: 1.0,
              margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
              child: Container(
                child: ListTile(
                  contentPadding:
                      EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                  title: Text(
                    "News and Promotions",
                    style: TextStyle(
                        color: Colors.black, fontWeight: FontWeight.bold),
                  ),
                  // subtitle: Text("test", style: TextStyle(color: Colors.white)),
                  trailing: new Switch(
                    value: newsPromotionsNotifications,
                    onChanged: (value) {
                      setState(() {
                        newsPromotionsNotifications = value;
                      });
                    },
                    activeTrackColor: Colors.lightGreenAccent,
                    activeColor: Colors.green,
                    inactiveTrackColor: Colors.grey,
                    inactiveThumbColor: Colors.grey,
                  ),
                ),
              ),
            ),
          ],
        ),
        appBar: GeneralAppBar("Notifications").setAppBar());
  }
}
