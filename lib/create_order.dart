/**
 * 
 * create_order.dart
 * 
 * Jason Haddad
 * 
 * This page provides users with the list of eateries for their campus. When one of them is clicked, it will bring them to that eateries 
 * page where they can select which items to order. Utilized by homepage.dart
 * 
 */

import 'package:crunch_flutter/values/eatery_ids.dart';
import 'package:flutter/material.dart';
import 'eatery_page.dart';

class CreateOrder extends StatefulWidget {
  //This comes from homepage.dart; its either the users last order or just text saying they dont have any orders.
  final Widget lastUserOrder;

  CreateOrder(this.lastUserOrder);

  @override
  State<StatefulWidget> createState() => new _CreateOrderState();
}

class _CreateOrderState extends State<CreateOrder> {
  //Used to map eatery ids to eatery names (since eateries are static 99% of the time, no need for a 'getEateries' API call and cache)
  //Will eventually be based on campus/collegee, will refactor accordingly
  var eateryIds = EateryIds.eateryIds;

  @override
  Widget build(BuildContext context) {
    var _width = MediaQuery.of(context).size.width / 2;
    return Container(
        color: Colors.transparent,
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Column(
            children: <Widget>[
              widget.lastUserOrder,
              Divider(  
              ),
              Padding(padding: const EdgeInsets.all(8.0)),
              Text("Sheridan College", style: TextStyle(fontSize: 20, decoration: TextDecoration.underline)),
              GridView.builder(
                shrinkWrap: true,
                physics: BouncingScrollPhysics(),
                itemCount: eateryIds.length,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2),
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      children: <Widget>[
                        GestureDetector(
                          //When eatery block is tapped, instantiate an eatery page with the given eatery name and eatery id
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (BuildContext context) => EateryPage(
                                    eateryIds.keys.toList()[index],
                                    eateryIds.values.toList()[index])));
                          },
                          child: Card(
                            elevation: 5,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8.0),
                            ),
                            child: Container(
                              width: _width,
                              height: _width - 50,
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(8.0),
                                child: FadeInImage(
                                  image:
                                      //TODO: Fix when we get scraped images
                                      AssetImage("assets/images/" +
                                          eateryIds.keys.toList()[index] +
                                          "_logo.jpg"),
                                  fit: BoxFit.cover,
                                  placeholder:
                                      AssetImage('assets/images/logo.png'),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Text(eateryIds.keys.toList()[index]),
                      ],
                    ),
                  );
                },
              ),
            ],
          ),
        ));
  }
}
