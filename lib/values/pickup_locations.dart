/*
 * 
 * pickup_locations.dart
 * 
 * Julian Mooken
 * 
 * 
 * 
 */

class PickupLocations {
  static var eateryLocationsLat = {
    "Tom Hortons": 43.468590,
    "Ty Express": 43.468012,
    "Carvey\'s": 43.467981,
    "Third Cup": 43.468962
    // "Playa\'s Pizza" : 6,
    // "Subward" : 7,
    // "The Sharquee" : 9,
    // "Espresso Loft" : 8
  };
  static var eateryLocationsLong = {
    "Tom Hortons": -79.700515,
    "Ty Express": -79.700614,
    "Carvey\'s": -79.700936,
    "Third Cup": -79.699716
    // "Playa\'s Pizza" : 6,
    // "Subward" : 7,
    // "The Sharquee" : 9,
    // "Espresso Loft" : 8
  };
  static var sheridanTrafalgarWingsLat = {
    "A Wing": 43.468388,
    "B Wing": 43.468419,
    "C Wing": 43.468649,
    "D Wing": 43.469081,
    "E Wing": 43.467805,
    "G Wing": 43.467229,
    "H Wing": 43.469399,
    "J Wing": 43.469694,
    "K Wing": 43.470155,
    "Commons": 43.4681987
  };

  static var sheridanTrafalgarWingsLong = {
    "A Wing": -79.701709,
    "B Wing": -79.700556,
    "C Wing": -79.698759,
    "D Wing": -79.700162,
    "E Wing": -79.699929,
    "G Wing": -79.700002,
    "H Wing": -79.700016,
    "J Wing": -79.698847,
    "K Wing": -79.698938,
    "Commons": -79.6992787
  };
}
