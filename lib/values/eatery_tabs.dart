/**
 * 
 * eatery_tabs.dart
 * 
 * Jason Haddad
 * 
 */

//TODO: not sure if this or a 'getCategories()' call OR just send categories with eateries

class EateryTabs {
  static List<String> getTabs(String eatery) {
    switch (eatery) {
      case "Tom Hortons":
        {
          return ["Hot Drinks", "Cold Drinks", "Baked Goods"];
        }

      case "Ty Express":
        {
          return ["Ty Food", "Promos", "Drinks"];
        }

      case "Third Cup":
        {
          return ["Beverages", "Food", "Better For You"];
        }

      // case "Carvey's":
      //   {
      //     return ["Burgers", "Chicken", "Sides", "Drinks", "Combos"];
      //   }

      // case "Playa's Pizza":
      // {
      //   return ["Pizzas", "Sides", "Drinks"];
      // }

      // case "Subward":
      // {
      //   return ["Subs", "Drinks", "Snacks"];
      // }

      // default:
      //   {
      //     return ["Cold Drinks", "Warm Drinks", "Meals", "Snacks"];
      //   }
    }
  }
}
