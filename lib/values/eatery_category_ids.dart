/**
 * 
 * eatery_category_ids.dart
 * 
 * Jason Haddad
 * 
 */

//TODO: not sure if this or a 'getCategories()' call OR just send categories with eateries

class EateryCategoryIds {
  static List<int> getCategoryIds(String eatery) {
    switch (eatery) {
      case "Tom Hortons":
        {
          return [5, 6, 7];
        }

      case "Ty Express":
        {
          return [8, 16, 32];
        }

      case "Third Cup":
        {
          return [17, 18, 20];
        }

      // case "Carvey's":
      //   {
      //     return [21, 22, 23, 24, 25];
      //   }

      // case "Playa's Pizza":
      // {
      //   return [26, 27, 28];
      // }

      // case "Subward":
      // {
      //   return [29, 30, 31];
      // }

      // default:
      // {
      //   return [5, 6, 7];
      // }
    }
  }
}
