/*
 * 
 * eatery_ids.dart
 * 
 * Jason Haddad
 * 
 * Our current Sheridan College Trafalgar eateries and their IDS. Instead of getting all eateries and caching data, we 
 * just map ids to eatery name (unsure if this is sound, ask kevin)
 * 
 */

class EateryIds {
  static var eateryIds= {
    "Tom Hortons" : 1,
    "Ty Express" : 2,
    // "Carvey\'s" : 5,
    "Third Cup" : 4,
    // "Playa\'s Pizza" : 6,
    // "Subward" : 7,
    // "The Sharquee" : 9,
    // "Espresso Loft" : 8
  };
}