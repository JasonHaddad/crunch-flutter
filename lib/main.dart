import 'dart:async';

/**
 * 
 * main.dart
 * 
 * Jason Haddad
 * 
 * This is the 'main' of a Flutter application. Here it is decided whether the user is logged in automatically (based on whether their
 * information is in SharedPreferences) or brought to the login page
 * 
 */

import 'package:crunch_flutter/forgot_password.dart';
import 'package:crunch_flutter/homepage.dart';
import 'package:crunch_flutter/login.dart';
import 'package:crunch_flutter/models/user.dart';
import 'package:crunch_flutter/registration.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:crunch_flutter/shared/user_details.dart';
import 'dart:convert';

void main() => runApp(ChangeNotifierProvider<ThemesNotifier>(
      child: MyApp(),
      builder: (BuildContext context) {
        return ThemesNotifier();
      },
    ));

enum MyThemes { light, dark }

class ThemesNotifier with ChangeNotifier {
  static final List<ThemeData> themeData = [
    ThemeData(
      brightness: Brightness.light,
      primaryColor:  Color.fromRGBO(0, 48, 86, 1.0),
      accentColor: Color.fromRGBO(0, 159, 189, 1.0),
    ),
    ThemeData(
        brightness: Brightness.dark,
        primaryTextTheme: TextTheme(button: TextStyle(color: Colors.black), body1: TextStyle(color: Colors.black)),
        primaryColor: Colors.grey,
        accentColor: Colors.black)
  ];

  MyThemes _currentTheme = MyThemes.light;
  ThemeData _currentThemeData = themeData[0];

  get currentTheme => _currentTheme;
  get currentThemeData => _currentThemeData;
  void switchTheme() => currentTheme == MyThemes.light
      ? currentTheme = MyThemes.dark
      : currentTheme = MyThemes.light;

  set currentTheme(MyThemes theme) {
    if (theme != null) {
      _currentTheme = theme;
      _currentThemeData =
          currentTheme == MyThemes.light ? themeData[0] : themeData[1];

      //Notifies the theme change to app
      notifyListeners();
    }
  }
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Crunch',
      theme: Provider.of<ThemesNotifier>(context).currentThemeData,
      routes: {
        '/home': (context) => MyHomePage(),
        '/login': (context) => MyLoginPage(),
        '/register': (context) => RegistrationPage(),
        // '/tracking': (context) => TrackingPage(),
        '/forgot-password': (context) => ForgotPassword(),
      },
      //Start at the splash screen
      home: SplashScreen(),
    );
  }
}

class SplashScreen extends StatelessWidget {
  //This method checks if SharedPrefs is populated (meaning the user has logged in before)
  Future checkForExistingLogin(BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.containsKey('user')) {
      //Set up our global user singleton like we did in the login class
      UserDetails userDetails = UserDetails();
      userDetails.user = User.fromJson(json.decode(prefs.getString("user")));

      return true;
    }
    return false;
  }

  @override
  Widget build(BuildContext context) {
    Future.delayed(const Duration(seconds: 7), () {
      checkForExistingLogin(context).then((loggedIn) {
        if (loggedIn) {
          Navigator.pushReplacementNamed(context, '/home');
        } else {
          Navigator.pushReplacementNamed(context, '/login');
        }
      });
    });

    return new Scaffold(
      body: new Card(
          child: new Center(
        child: Image.asset(
          "assets/images/logo_anim.gif",
          height: 250.0,
          width: 250.0,
        ),
      )),
    );
  }
}
