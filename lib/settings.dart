import 'package:crunch_flutter/main.dart';
/**
 * 
 * setings.dart
 * 
 * Julian Mooken
 * 
 * This page houses links to multiple settings pages that each set up different things
 * 
 */

import 'package:crunch_flutter/settings_account.dart';
import 'package:crunch_flutter/settings_notifications.dart';
import 'package:crunch_flutter/settings_payment.dart';
import 'package:crunch_flutter/shared/general_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'shared/app_drawer.dart';

class Settings extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new _SettingsState();
}

class _SettingsState extends State<Settings> {
  void changeToDark() {}

  var _darkMode = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          SwitchListTile(
            title: const Text('Dark mode'),
            value: _darkMode,
            onChanged: (bool value) {
              setState(() {
                _darkMode = !_darkMode;
                Provider.of<ThemesNotifier>(context).switchTheme();
              });
            },
            secondary: const Icon(Icons.lightbulb_outline),
          ),
          GestureDetector(
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (BuildContext context) => SettingsAccount()));
            },
            child: Card(
              elevation: 1.0,
              margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
              child: Container(
                //decoration: BoxDecoration(color: Colors.white),
                child: ListTile(
                    contentPadding:
                        EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                    title: Text(
                      "Account",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    trailing: Icon(Icons.account_box, size: 30.0)),
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (BuildContext context) => SettingsPayment()));
            },
            child: Card(
              elevation: 1.0,
              margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
              child: Container(
                //decoration: BoxDecoration(color: Colors.white),
                child: ListTile(
                    contentPadding:
                        EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                    title: Text(
                      "Payment",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    trailing: Icon(Icons.payment, size: 30.0)),
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (BuildContext context) => SettingsNotifications()));
            },
            child: Card(
              elevation: 1.0,
              margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
              child: Container(
                //decoration: BoxDecoration(color: Colors.white),
                child: ListTile(
                    contentPadding:
                        EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                    title: Text(
                      "Notifications",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    trailing: Icon(Icons.notifications_active, size: 30.0)),
              ),
            ),
          ),
          const SizedBox(height: 30),

          //This logout button clears our user from shared preferences and eliminates the entire navigation stack so user
          //cannot click back button to go back into application
          RaisedButton(
            onPressed: () async {
              SharedPreferences prefs = await SharedPreferences.getInstance();
              prefs.remove('user');
              Navigator.pushNamedAndRemoveUntil(
                  context, '/login', (_) => false);
            },
            child: const Text('Logout', style: TextStyle(fontSize: 20)),
          ),
        ],
      ),
      appBar: AppBar(title: Text("Settings"), centerTitle: true),
    );
  }
}
