# crunch_flutter

This project is for our FRONT-END mobile development work

To build and run this project, proceed as follows (ON WINDOWS):

	1. Download the Flutter SDK to your system (https://flutter.dev/docs/get-started/install/windows) 

	2. From the downloaded zip file, extract the 'flutter' folder to a location of your choice (e.g. C:\sdks\flutter)

	3. Edit the PATH environment variable that points to the 'bin' folder in the 'flutter' folder you just extracted (in order to run flutter commands in command prompt)

	4. You then must download Android Studio as it is necessary for flutter to support Android dependencies. Simply downlaoding Android Studio will satisfy these requirements


We utilized VS Code to develop our project as it was extremely simple and straightforward to use. To get it set up, proceed as follows:

	1. Download bothe the Flutter and Dart extensions for VS code

	2. This will likely prompt you to point the Flutter extension to the flutter sdk you downloaded earlier, otherwise you cando so in the Flutter extension page

**If you are using an android device to build the application on, be sure to enable 'USB Debugging' otherwise it wont allow the sideloading of applications.**

**You can also use an android emulator as well**

	3. After cloning the repo on your machine, simply open up a windows terminal inside VS code (or a generic command prompt after moving to project directory), choose your device from the bottom right tab on VS Code,  and type 'flutter run'

	4. This will build the applicaiton on your chosen device after some time, and it will run. 

**Refer to https://flutter.dev/docs/get-started/install/windows if there's anything amiss**

Team Momus - Jason Haddad, Julian Mooken, Daniel Hope, John Le
